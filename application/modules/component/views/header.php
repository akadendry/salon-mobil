<!DOCTYPE HTML>
<html>
<head>
<title>DSG Indonesia</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Roadster Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />	
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"> </script> 
<!--webfonts-->
 <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
 <link href='//fonts.googleapis.com/css?family=Raleway:400,500' rel='stylesheet' type='text/css'>

<link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

<!--//webfonts-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/easing.js"></script>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>

</head>
<body>
	<!--start-home-->
<div id="home" class="header">
		<div class="container">
	    <div class="header-top">
	        <div class="logo">
	<p style="text-align: left;"><a href="<?php echo base_url();?>home"><img style="width: 90%;" src="<?php echo base_url();?>assets/images/logo.png"></a></p>
				</div>
					<div class="h-right">
					  
						<div class="working-hours phone-number">
							
							<div class="phone-text"><a class="active" href="<?php echo base_url();?>login" data-hover="HOME"><img src="<?php echo base_url();?>assets/images/login.png" style="width: 9%;"> LOGIN</a>
							
							 </div>
							 <div class="clearfix"> </div>
						 </div>
						 <div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
					<!-- <div class="quate">
									<a href="#" class="hvr-bounce-to-bottom m1">Get a Quote</a>
								</div> -->
					<span class="menu"></span>
				    <div class="top-menu">
								 
								 
							<ul class="cl-effect-16">
								
								
								<li><a href="#" data-hover="SERVICES">Services</a></li>
								<li><a href="#" data-hover="Gallery">Gallery</a></li>
								<li><a href="<?php echo base_url();?>about" data-hover="About">About</a></li>
								<li><a href="#" data-hover="CONTACT">Contact</a></li>
							</ul>
						</div>
							<!--script-for-menu-->
							<script>
							$( "span.menu" ).click(function() {
							  $( ".top-menu" ).slideToggle( "slow", function() {
								// Animation complete.
							  });
							});
						</script>
				<!-- start-search-->