<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>DSG Indonesia</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="DSG INDONESIA Detailing Shop Garage Ciputat, car detailing specialist" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <script>
        window.setTimeout(function () {
            $(".alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 3000);
    </script>
    <!-- //Meta tag Keywords -->
    <!-- Custom-Files -->
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap1.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style1.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="<?php echo base_url();?>/assets/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

    <!-- //Fonts -->
</head>

<body>
    <!-- mian-content -->
    <div class="main-content" id="home">
        <div class="layer" style="padding-bottom: 70px;">
            <!-- header -->
            <header>
                <?php if ($this->session->flashdata('message')): ?>
                    <div class="alert alert-<?=$this->session->flashdata('status');?>">
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php endif;?>
                <div class="container-fluid px-lg-5">
                    <!-- nav -->
                    <nav class="py-4 d-lg-flex">
                        <div class="logo">
                            <a href="<?php echo base_url();?>home"><img class="logo" src="<?php echo base_url();?>assets/images/logopolos.png"></a>
                        </div>
                        <label for="drop" class="toggle">Menu</label>
                        <input type="checkbox" id="drop" />
                        <ul class="menu mt-2 ml-auto">
                            <li class="#services"><a href="<?php echo base_url();?>about">Services</a></li>
                            
                             <li><a href="#blog" Dlass="scroll">Gallery</a></li>
                            <li><a href="#about" class="scroll">About</a></li>
                            <li><a href="#contact" class="scroll">Contact</a></li>
                            <?php if($this->session->userdata("email")):?>
                                  <li>
                                <!-- First Tier Drop Down -->
                                <label for="drop-2" class="toggle">Hi, <?=$this->session->userdata("name")?> <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
                                <a href="#">Hi, <?=$this->session->userdata("name")?> <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                                <input type="checkbox" id="drop-2" />
                                <ul class="inner-ul">
                                    <li><a class="scroll" href="<?= base_url('dashboard') ?>">Dashboard</a></li>
                                    <li><a href="<?php echo base_url();?>login/process_logout" class="scroll">Log Out</a></li>
                            
                                </ul>
                            </li>
                                
                            <?php else:?>
                                <li><a href="<?php echo base_url();?>login" class="scroll">Login</a></li>
                            <?php endif;?>
                        </ul>
                    </nav>
                    <!-- //nav -->
                </div>
            </header>
            <!-- //header -->
            <div class="container">
                <!-- /banner -->
                <div class="banner-info-w3layouts">
                    <img class="banner-info-w3layouts"src="<?php echo base_url();?>assets/images/logo_abu.png"style="margin:0;">
                    <h3>Because we know how much you love your car.</h3>
                    <?php if($this->session->userdata("email")):?>
                        <p class="my-3">Lorem Ipsum has been the industry's standard since the 1500s.Vestibulum ante ipsum primis in faucibus orci luctus.</p> <a href="<?php echo base_url();?>book" class="read-more mt-3 btn">Book Now</a> </div>
                    <?php else:?>
                        <p class="my-3">Lorem Ipsum has been the industry's standard since the 1500s.Vestibulum ante ipsum primis in faucibus orci luctus.</p> <a href="<?php echo base_url();?>login" class="read-more mt-3 btn">Book Now</a> </div>
                    <?php endif;?>
            </div>
        </div>
        <!-- //banner -->
    </div>
    <!--// mian-content -->
    <!-- banner-bottom-wthree -->
    
    <!-- //banner-bottom-wthree -->
   
   
   
    <!-- //footer -->

<script src="assets/js/jquery-2.1.4.min.js"></script>
</body>

</html>
