<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Register');
    }

    public function index() {
        $this->load->view('register');
    }

    public function add() {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $password = $this->input->post('password');
        //cek user yet
        $array_data = array(
            'email' => $email
        );
        $check = $this->M_Register->check_user_yet($array_data)->result();
        if (count($check) > 0) {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Email yang didaftarkan sudah pernah terdaftar!');
            return redirect('register');
        }
        //insert data user
        $data_insert = array(
            'name' => $name,
            'email' => $email,
            'password' => sha1(md5($password)),
            'phone' => $phone,
            'email_token' => sha1(md5($email)),
            'verified' => 0,
            'created_at' => date('Y-m-d H:i:s')
        );
        $insert_data = $this->M_Register->insert($data_insert);
        if ($insert_data == 1) {
            //kirim email
            $config = Array(
                'smtp_host' => 'mail.detailingshopgarage.com',
                'smtp_port' => 465,
                'smtp_user' => 'no-reply@detailingshopgarage.com', // change it to yours
                'smtp_auth' => TRUE,
                'smtp_pass' => 'Password1', // change it to yours
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'newline' =>  "\r\n",
                'wordwrap' => TRUE
            );
//
//            $message = 'hello';
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('no-reply@detailingshopgarage.com', 'DSG Indonesia');
            $this->email->to($email); // change it to yours
            $this->email->subject('Pendaftaran User Detailing Shop Garage');
            $data['recipient_email'] = $email;
            $data['url'] = base_url() . 'register/verification_email/' . sha1(md5($email));
            $data['subjek_email'] = "Pendaftaran User";
            $email = $this->load->view('v_email_template_pendaftaran_user', $data, TRUE);
            $this->email->message($email);
            if ($this->email->send()) {
                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('message', 'Berhasil daftar user, Silahkan cek email untuk verifikasi akun');
                return redirect('login');
            } else {
                show_error($this->email->print_debugger());
            }
//            $this->email->from('detailingShopGarage@gmail.com');
//            $this->email->to($email);
//            $this->email->subject('Pendaftaran User Detailing Shop Garage');
//            $data['recipient_email'] = $email;
//            $data['url'] = base_url() . 'register/verification_email/' . sha1(md5($email));
//            $data['subjek_email'] = "Pendaftaran User";
//            $email = $this->load->view('v_email_template_pendaftaran_user', $data, TRUE);
//            $this->email->to($email);
//            $this->email->message($email);
////            $this->email->send();
//            if ($this->email->send()) {
//                echo 'Your email was sent, thanks chamil.';
//            } else {
//                show_error($this->email->print_debugger());
//            }
//            exit();
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('register');
        }
        exit();
    }

    public function verification_email() {
        $email_sha1 = $this->uri->segment(3);
        $array_data = array(
            'email_token' => $email_sha1
        );
        $check = $this->M_Register->check_user_yet($array_data)->result();
        if (count($check) > 0) {
            foreach ($check as $row):
                $id = $row->id_user;
            endforeach;
            $data_update = array(
                'verified' => 1
            );
            $updated = $this->M_Register->updated($data_update, $id);
            if ($updated == 1) {
                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('message', 'Akun berhasil active, silahkan login terlebih dahulu');
                return redirect('login');
            } else {
                $this->session->set_flashdata('status', 'danger');
                $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
                return redirect('login');
            }
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('login');
        }
    }

}
