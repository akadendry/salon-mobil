<?php

class M_Register extends CI_Model {

    public function check_user_yet($where) {
        return $this->db->get_where('mst_users',$where);
    }
    
    public function insert($data_insert){
        $query = $this->db->insert('mst_users', $data_insert); 
        if($query){
            return 1;
        }else{
            return 0;
        }
    }
    
    public function updated($data_update, $id){
        $this->db->where('id_user', $id);
        $query = $this->db->update('mst_users', $data_update); 
        if($query){
            return 1;
        }else{
            return 0;
        }
    }

}
