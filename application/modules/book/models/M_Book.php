<?php

class M_Book extends CI_Model {

    public function insert($data_insert) {
        $query = $this->db->insert('booking_temp', $data_insert);
        if ($query) {
            return $this->db->insert_id();
        } else {
            return 0;
        }
    }
    
    public function getAllListServvice() {
        $data = $this->db->query("SELECT *
                FROM mst_service a 
                WHERE is_active = 1 and deleted_at is null
                ORDER BY id_service ASC");
        return $data->result();
    }
    
    public function getAllListServviceAvailable() {
        $data = $this->db->query("SELECT *
                FROM mst_service a 
                WHERE is_active = 1
                ORDER BY id_service ASC");
        return $data->result();
    }
    
    public function getAllDataCompany() {
        $data = $this->db->query("SELECT *
                FROM mst_company
                WHERE deleted_at is null
                ORDER BY id ASC");
        return $data->result();
    }
    
    public function getDataPriceByIdService($idService, $size_id) {
        $data = $this->db->query("SELECT *
                FROM mst_price
                WHERE is_active = 1 AND id_service = '$idService' AND id_size = '$size_id'
                ORDER BY id_size ASC");
//        WHERE is_active = 1 AND id_service = '$idService' AND id_size != 2 AND id_size !=3
        return $data->result();
    }
    
    public function getAllDataBrand() {
        $data = $this->db->query("SELECT *
                                  FROM mst_brand 
                                  WHERE deleted_at is null
                                  ORDER BY name ASC");
        return $data->result();
    }
    
    public function getTypeCarByBrand($brand_id) {
        $data = $this->db->query("SELECT a.*, b.name as brand_name, c.name_size
                                  FROM mst_brand_type a
                                  JOIN mst_brand b ON a.brand_id = b.id
                                  JOIN car_size c ON a.size_id = c.id_size
                                  WHERE a.brand_id = '$brand_id'
                                  ORDER BY a.name ASC");
        return $data->result();
    }
    
    public function getSizeByType($brand_id) {
        $data = $this->db->query("SELECT a.*, b.name as brand_name, c.name_size
                                  FROM mst_brand_type a
                                  JOIN mst_brand b ON a.brand_id = b.id
                                  JOIN car_size c ON a.size_id = c.id_size
                                  WHERE a.id = '$brand_id'
                                  ORDER BY a.name ASC");
        return $data->result();
    }
    
    public function getAllDataService() {
        $data = $this->db->query("SELECT * 
                                  FROM mst_service  
                                  WHERE is_active = 1 AND deleted_at is null
                                  ORDER BY id_service DESC");
        return $data->result();
    }
    
    public function getAllDataServiceMerge($company_id) {
        $data = $this->db->query("SELECT a.*, b.*
                                  FROM service_margin_price a
                                  LEFT JOIN mst_service b ON a.service_id = b.id_service
                                  WHERE b.is_active = 1 AND a.deleted_at is null AND a.company_id = $company_id
                                  ORDER BY b.id_service DESC");
        return $data->result();
    }
    
    public function getAllDataPriceMargin($company_id, $service_id, $size_id) {
        $data = $this->db->query("SELECT a.*, b.name_service, c.name_size, (a.price * d.margin / 100) as price_total
                                  FROM mst_price a
                                  JOIN mst_service b ON a.id_service = b.id_service
                                  JOIN car_size c ON a.id_size = c.id_size
                                  RIGHT JOIN service_margin_price d ON b.id_service = d.service_id
                                  WHERE d.deleted_at is null AND d.company_id = $company_id AND b.id_service = $service_id AND a.id_size = '$size_id'
                                  ORDER BY a.id_price DESC");
        return $data->result();
    }
}
