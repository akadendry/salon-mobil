<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Book');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {
        $this->data['listServcie'] = $this->M_Book->getAllListServvice();
        $this->data['listBrand'] = $this->M_Book->getAllDataBrand();
        $this->data['listCompany'] = $this->M_Book->getAllDataCompany();
//        $tanggalAvailable = array();
        $tanggal = '[';
        $now = date('Y-m-d');
        for ($i = 0; $i <= 60; $i++) {
            $tanggalTerakhirbooking = date('Y-m-d', strtotime($now . ' +' . $i . ' days')) . '/5';
            if ($i == 60) {
                $tanggal .= '"' . $tanggalTerakhirbooking . '"';
            } else {
                $tanggal .= '"' . $tanggalTerakhirbooking . '",';
            }
        }
        $tanggal .= ']';
//        echo $tanggal;
//        exit();
        $this->data['tanggalBookingAvailable'] = $tanggal;
        $this->load->view('book', $this->data);
    }

    public function order() {
//        print_r($this->input->post());
//        exit();
        $this->load->helper('form');
        $email = $this->input->post('email');
//        $now = date('H:i');
        $timestamp = strtotime(date('H:i')) + 60 * 60;
        $expireDateDp = date('Y-m-d H:i:s', $timestamp);
        $price = str_replace("Rp. ", "", $this->input->post('price'));
        $price = str_replace(",", "", $price);
        $data = array(
            'id_user' => $this->input->post('id_user'),
            'no_polisi' => $this->input->post('plat_no'),
            'car_brand' => $this->input->post('car_brand'),
            'car_type' => $this->input->post('car_type'),
            'car_color' => $this->input->post('car_color'),
            'name_service' => $this->input->post('name_service'),
            'book_date' => date('Y-m-d H:i:s', strtotime($this->input->post('book_date'))),
            'id_size' => $this->input->post('size'),
            'id_price' => $this->input->post('price_id'),
            'price' => $price,
            'type_booking' => 'online',
            'status_inspect' => 'belum',
            'created_by' => $this->input->post('id_user'),
            'created_at' => date('Y-m-d'),
            'expireDateDp' => $expireDateDp,
            'statusDp' => 0,
            'dp' => $this->input->post('dp'),
            'company_id' => $this->input->post('company_id')
        );
        $insert_data = $this->M_Book->insert($data);
        if ($insert_data == 0) {
            //kirim email
//            $this->load->library('email');
////            $this->config->load('email', TRUE);
//            
//            $this->email->clear();
//            
//            $this->email->clear();
//
//            $this->email->to($email);
//            $this->email->from('detailingShopGarage@gmail.com');
//            $this->email->subject('Pendaftaran User Detailing Shop Garage');
//            $data['recipient_email'] = $email;
//            $data['url'] = base_url() . 'Register/verification_email/' . sha1(md5($email));
//            $data['subjek_email'] = "Pendaftaran User";
//            $email = $this->load->view('v_email_template_pendaftaran_user', $data, TRUE);
////            $this->email->to($email);
//            $this->email->message($email);
//            $this->email->send();
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('book');
        } else {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Booking berhasil, silahkan melakukan pembayaran');
            return redirect('dashboard/paymentMethod/' . $insert_data);
        }
    }

    public function getDataPriceByIdService($idService, $size_id) {
        $tmp = '';
        $data = $this->M_Book->getDataPriceByIdService($idService, $size_id);
        if (!empty($data)):
            $tmp .= "Price : ";
            foreach ($data as $key => $row):
                $hargaAwal = number_format($row->price);
                $tmp .= 'Rp.' . $hargaAwal;
            endforeach;
            $tmp .= "<br>Booking DP : Rp.500,000";
        else:
            $tmp .= "Data Harga Tidak Ditemukan Harap Hubungi Admin Kami";
        endif;
        die($tmp);
    }

    public function getPrice() {
        $tmp = '';
        $service_id = $_POST['service_id'];
        $size_id = $_POST['size_id'];
        $company_id = $_POST['company_id'];
        if ($company_id == 1) {
            $get_price = $this->M_Book->getDataPriceByIdService($service_id, $size_id);
            if (count($get_price) > 0):
                foreach ($get_price as $row):
                    $hargaAwal = number_format($row->price);
                    $hargabooking = $row->price * 30 / 100;
                    $tmp .= 'Price : Rp.' . $hargaAwal;
                    $tmp .= "<br>Booking DP : Rp. " . number_format($hargabooking);
                    $arr = array(
                        'status' => 'success',
                        'price_id' => $row->id_price,
                        'dp' => $hargabooking,
                        'price' => 'Rp. ' . number_format($row->price),
                        'tmp' => $tmp,
                        'message' => $hargabooking
                    );
                endforeach;
            else:
                $arr = array(
                    'status' => 'failed',
                    'price_id' => '',
                    'price' => 'Rp. ',
                    'tmp' => 'Harga belum diatur untuk type mobil dan service tersebut, laporkan kepada admin!',
                    'message' => 'Harga belum diatur untuk type mobil dan service tersebut, laporkan kepada admin!'
                );
            endif;
        }else {
            $get_price = $this->M_Book->getAllDataPriceMargin($company_id, $service_id, $size_id);
            if (count($get_price) > 0):
                foreach ($get_price as $row):
                    $total_keseluruhan = $row->price + $row->price_total;
                    $hargaAwal = number_format($total_keseluruhan);
                    $hargabooking = $total_keseluruhan * 30 / 100;
                    $tmp .= 'Price : Rp.' . $hargaAwal;
                    $tmp .= "<br>Booking DP : Rp. " . number_format($hargabooking);
                    $arr = array(
                        'status' => 'success',
                        'price_id' => $row->id_price,
                        'dp' => $hargabooking,
                        'price' => 'Rp. ' . number_format($total_keseluruhan),
                        'tmp' => $tmp,
                        'message' => $hargabooking
                    );
                endforeach;
            else:
                $arr = array(
                    'status' => 'failed',
                    'price_id' => '',
                    'price' => 'Rp. ',
                    'tmp' => 'Harga belum diatur untuk type mobil dan service tersebut, laporkan kepada admin!',
                    'message' => 'Harga belum diatur untuk type mobil dan service tersebut, laporkan kepada admin!'
                );
            endif;
        }

        echo json_encode($arr);
    }

    public function getTypeCarByBrand() {
        $brand_id = $_POST['merk_id'];
        $option = '<option value="">-- Choose Your Car Type --</option>';
        $get_type_car_by_brand = $this->M_Book->getTypeCarByBrand($brand_id);
        foreach ($get_type_car_by_brand as $row):
            $option .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        endforeach;
        echo $option;
    }

    public function getSizeByType() {
        $type_id = $_POST['type_id'];
        $get_size_by_type = $this->M_Book->getSizeByType($type_id);
        if (count($get_size_by_type) > 0):
            foreach ($get_size_by_type as $row):
                $val = $row->size_id;
            endforeach;
        else:
            $val = '';
        endif;
        echo $val;
    }

    public function getServiceByCompany() {
        $company_id = $_POST['company_id'];
        $option = '<option value="">-- Choose Your Service --</option>';
        if ($company_id == 1) {
            $get_sevice_by_company = $this->M_Book->getAllDataService();
        } else {
            $get_sevice_by_company = $this->M_Book->getAllDataServiceMerge($company_id);
        }
        foreach ($get_sevice_by_company as $row):
            $option .= '<option value="' . $row->id_service . '">' . $row->name_service . '</option>';
        endforeach;
        echo $option;
    }

}
