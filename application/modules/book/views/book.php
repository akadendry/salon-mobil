<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>DSG - Book For Service</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>/assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/favicon.png" />

        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

        <!-- CSS Files -->
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap1.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>/assets/css/material-bootstrap-wizard1.css" rel="stylesheet" />

        <!--css tambahan untuk tanggal-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/aceAdmin/fullcalendar.min.css" />
        <style>
            .fc-highlight {
                background: blue !important;
                opacity: inherit;
            }
        </style>

        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="<?php echo base_url(); ?>/assets/css/demo.css" rel="stylesheet" />
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body>
        <div class="image-container set-full-height" style="background-image: url('assets/images/bg.png')">
            <!--   Creative Tim Branding   -->
            <a href="<?php echo base_url(); ?>home">
                <div class="logo-container">
                    <div class="logo">
                        <img src="<?php echo base_url(); ?>/assets/images/logo_abu.png">
                    </div>

                </div>
            </a>


            <!--   Big container   -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <!--      Wizard container        -->
                        <div class="wizard-container">
                            <div class="card wizard-card" data-color="red" id="wizard">
                                <?php if ($this->session->flashdata('message')): ?>
                                    <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                                        <?php echo $this->session->flashdata('message'); ?>
                                    </div>
                                <?php endif; ?>
                                <form action="<?= base_url('book/order') ?>" method="post">
                                    <!--        You can switch " data-color="blue" "  with one of the next bright colors: "green", "orange", "red", "purple"             -->

                                    <div class="wizard-header">
                                        <h3 class="wizard-title">
                                            Book a Service
                                        </h3>
                                        <h5>This information will let us know more about your car.</h5>
                                    </div>
                                    <div class="wizard-navigation">
                                        <ul class="test">
                                            <li><a href="#details" data-toggle="tab">Car Detail</a></li>
                                            <li><a href="#location" data-toggle="tab">Location</a></li>
                                            <li><a href="#captain" data-toggle="tab">Service Type</a></li>
                                            <li><a href="#description" data-toggle="tab">Extra Details</a></li>
                                        </ul>
                                    </div>

                                    <div class="tab-content">
                                        <div class="tab-pane" id="details">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h4 class="info-text"> Let's start with the basic details.</h4>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">directions_car</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <!--<label class="control-label">Your Car Brand</label>-->
                                                            <input name="id_user" type="hidden" value="<?= $this->session->userdata("id") ?>" class="form-control">
                                                            <input name="email" type="hidden" value="<?= $this->session->userdata("email") ?>" class="form-control">
                                                            <!--<input name="car_brand" type="text" class="form-control" required>-->
                                                            <select name="car_brand" id="merk" class="form-control" required>
                                                                <option value="">-- Choose Your Car Brand --</option>
                                                                <?php foreach ($listBrand as $lb): ?>
                                                                    <option value="<?= $lb->id ?>"><?= $lb->name ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">directions_car</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <!--<label class="control-label">Your Car Type</label>-->
                                                            <!--<input name="car_type" type="text" class="form-control" required>-->
                                                            <select name="car_type" id="type" class="form-control" required>
                                                                <option value="">-- Choose Your Car Type --</option>
                                                            </select>
                                                            <input hidden type="text" name="size" id="size"/>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">directions_car</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Your Car Color</label>
                                                            <input name="car_color" type="text" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">directions_car</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Plat Number</label>
                                                            <input name="plat_no" type="text" class="form-control" required>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="form-group label-floating">
                    <label class="control-label">Daily Budget</label>
                    <select class="form-control">
                                                                    <option disabled="" selected=""></option>
                    <option value="Afghanistan"> < $100 </option>
                    <option value="Albania"> $100 - $499 </option>
                    <option value="Algeria"> $499 - $999 </option>
                    <option value="American Samoa"> $999+ </option>
                    </select>
            </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="location">
                                            <h4 class="info-text">Select Our Location </h4>
                                            <div class="row">
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="col-sm-12">
                                                        <select name="company_id" id="selectCompany" class="form-control" required>
                                                            <option value="">-- Silahkan Pilih Lokasi Service --</option>
                                                            <?php foreach ($listCompany as $lc): ?>
                                                                <option value="<?= $lc->id ?>"><?= $lc->name ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="captain">
                                            <h4 class="info-text">Select Our Services </h4>
                                            <div class="row">
                                                <div class="col-sm-10 col-sm-offset-1">
                                                    <div class="col-sm-12">
                                                        <select name="name_service" id="selectService" class="form-control service" required>
                                                            <option value="">-- Silahkan Pilih Service Yang Anda Inginkan --</option>
                                                            <?php foreach ($listServcie as $row): ?>
                                                                <option value="<?= $row->id_service ?>"><?= $row->name_service ?></option>
                                                            <?php endforeach; ?>
                                                            <input type="hidden" readonly id="price" name="price" class="width-100" />
                                                            <input type="hidden" readonly id="price_id" name="price_id" class="width-100" />
                                                            <input type="hidden" readonly id="dp" name="dp" class="width-100" />
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-10 col-sm-offset-1" style="margin-left: 80px;">
                                                    <div id="dataPriceService"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--yang lama punya-->
                                        <!--                                        <div class="tab-pane" id="captain">
                                                                                    <h4 class="info-text">Select Our Services </h4>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-10 col-sm-offset-1">
                                                                                            <div class="col-sm-4">
                                                                                                <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="Protect Your Lovely Car With Premium Coat">
                                                                                                <input type="radio" name="name_service" id="name_service" value="Paint Protection Coat">
                                                                                                <div class="icon">
                                                                                                    <i class="material-icons">local_car_wash</i>
                                                                                                </div>
                                                                                                <h6>Paint Protection Coat</h6>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="Perfect For Cleaning Your Car Gears">
                                                                                                <input type="radio" name="name_service" id="name_service" value="Coat By Detailing Gear">
                                                                                                <div class="icon">
                                                                                                    <i class="material-icons">local_car_wash</i>
                                                                                                </div>
                                                                                                <h6>Coat By Detailing Gear</h6>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-sm-4">
                                                                                                <div class="choice" data-toggle="wizard-radio" rel="tooltip" title="Try The Kenzo">
                                                                                                <input type="radio" name="name_service" id="name_service" value="Coat By Kenzo">
                                                                                                <div class="icon">
                                                                                                    <i class="material-icons">local_car_wash</i>
                                                                                                </div>
                                                                                                <h6>Coat By Kenzo</h6>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>-->
                                        <div class="tab-pane" id="description">
                                            <!--                                            <div class="row">
                                                                                            <h4 class="info-text">Select Our Place </h4>
                                                                                            <div class="row">
                                                                                                <div class="col-sm-10 col-sm-offset-1">
                                                                                                    <div class="col-sm-12">
                                                                                                        <select name="company_id" id="selectCompany" class="form-control" required>
                                                                                                            <option value="">-- Silahkan Pilih Tempat Service --</option>
                                            </?php foreach ($listCompany as $lc): ?>
                                                                                                                    <option value="</?= $lc->id ?>"></?= $lc->name ?></option>
                                            </?php endforeach; ?>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <br><br>-->
                                            <div class="row" id="calender_book">
                                                <h4 class="info-text"> Choose Your Booking Date.</h4>
                                                <div class="col-sm-11" style="padding-left: 25px;">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="material-icons">&nbsp;</i>
                                                            <i class="material-icons" hidden>calendar_today</i>
                                                        </span>
                                                        <div class="form-group label-floating">
                                                            <div class="row">
                                                                <div class="col" style="text-align: center;">
                                                                    <div id="calendar" style="background-color: white;"></div>
                                                                </div>
                                                            </div>
                                                            <input id="selected_date" name="book_date" type="hidden" value="" />
                                                            <!--                                                            <label class="label">Book Date</label>
                                                                                                                        <input name="book_date" type="date" class="form-control">-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wizard-footer">
                                        <div class="pull-right">
                                            <input id="btnNext" type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next' value='Next' />
                                            <div id="button_page" style="display: none;">
                                                <input type='submit' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish' value='Finish' />
                                            </div>
                                        </div>
                                        <div class="pull-left">
                                            <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />

                                            <div class="footer-checkbox">
                                                <div class="col-sm-12">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div> <!-- wizard container -->
                    </div>
                </div> <!-- row -->
            </div> <!--  big container -->

            <div class="footer">
                <div class="container text-center">
                    Copyright DSGINDONESIA 2019
                </div>
            </div>
        </div>

    </body>
    <!--   Core JS Files   -->
    <script src="<?php echo base_url(); ?>/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.bootstrap.js" type="text/javascript"></script>

    <!--  Plugin for the Wizard -->
    <script src="<?php echo base_url(); ?>/assets/js/material-bootstrap-wizard.js"></script>

    <!--js untuk full calender-->
    <script src="<?php echo base_url(); ?>/assets/js/aceAdmin/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/aceAdmin/fullcalendar.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var date = new Date();
            var m = '' + (date.getMonth() + 1);
            var d = '' + date.getDate();
            var y = date.getFullYear();

            if (m.length < 2)
                m = '0' + m;
            if (d.length < 2)
                d = '0' + d;
            var full_date_now = [y, m, d].join('-');
            // console.log(m);
            var hourNow = date.getHours();
            var minutuesNow = date.getMinutes();
            var secondNow = date.getSeconds();
            if (hourNow.length < 2)
                hourNow = '0' + hourNow;
            if (minutuesNow.length < 2)
                minutuesNow = '0' + minutuesNow;
            if (secondNow.length < 2)
                secondNow = '0' + secondNow;
            var full_time_now = hourNow + ':' + minutuesNow + ':' + secondNow;

//            var avail = ["2020-02-01/5", "2020-02-02/5", "2020-02-03/5", "2020-02-04/5", "2020-02-05/5"];
            var avail = <?= $tanggalBookingAvailable ?>;
            //console.log(avail);

            $('#calendar').fullCalendar({
                monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
                dayNames: ['Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Ming'],
                dayNamesShort: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
                dayNamesColors: 'red',
                defaultDate: '2022-03-20',
                //isRTL: true,
                //firstDay: 1,// >> change first day of week 

                buttonHtml: {
                    prev: '<i class="ace-icon fa fa-chevron-left"></i>',
                    next: '<i class="ace-icon fa fa-chevron-right"></i>'
                },

                header: {
                    left:'prev,next,  today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },

                /**eventResize: function(event, delta, revertFunc) {
                 
                 alert(event.title + " end is now " + event.end.format());
                 
                 if (!confirm("is this okay?")) {
                 revertFunc();
                 }
                 
                 },*/

                //editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function (date) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    var $extraEventClass = $(this).attr('data-class');


                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = false;
                    if ($extraEventClass)
                        copiedEventObject['className'] = [$extraEventClass];

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                },
                selectable: true,
                // selectHelper: true,

                dayRender: function (date, cell) {
                    var raredate = new Date(date),
                            dbulan = '' + (raredate.getMonth() + 1),
                            dhari = '' + raredate.getDate(),
                            dtahun = raredate.getFullYear();

                    if (dbulan.length < 2)
                        dbulan = '0' + dbulan;
                    if (dhari.length < 2)
                        dhari = '0' + dhari;
                    fullDate = [dtahun, dbulan, dhari].join('-');
                    // console.log(fullDate);
                    if (date.isAfter(moment())) {
                        cell.css("background-color", "white");
                    }
                    if (moment().diff(date, 'days') > 0) {
                        cell.css("background-color", "white");
                    }
                    avail.forEach(function (avdate) {
                        var splitData = avdate.split("/");
                        var availableDate = splitData[0];
                        // console.log(availableDate);
                        var quotaAvail = splitData[1];
                        // alert (availableDate);
                        if (fullDate == availableDate) {
                            if (quotaAvail > 0) {
                                if (full_date_now === availableDate) {
                                    if (full_time_now > "13:00:00") {
                                        cell.css("background-color", "#FFFFFF");
                                    } else {
                                        cell.css("background-color", "green");
                                    }
                                } else {
                                    cell.css("background-color", "green");
                                }

                            } else {
                                cell.css("background-color", "red");

                            }

                        }
                    });
                },

                select: function (start, end, allDay) {
                    document.getElementById("button_page").style.display = 'none';
                    var d = new Date(start),
                            month = '' + (d.getMonth() + 1),
                            day = '' + d.getDate(),
                            year = d.getFullYear();

                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;
                    start = [year, month, day].join('-');
                    avail.forEach(function (avdate) {
                        var splitData = avdate.split("/");
                        var availableDate = splitData[0];
                        var quotaAvail = splitData[1];
                        if (start == availableDate) {
                            // $('#calendar').fullCalendar('select');
                            // $(this).css('background-color', 'black');
                            // alert(start);
                            // start.css("background-color", "blue");
                            if (quotaAvail > 0) {
                                if (full_date_now === availableDate) {
                                    if (full_time_now > "13:00:00") {
                                        $('#calendar').fullCalendar('unselect');
                                        alert('Sudah melewati batas waktu booking pada hari ini');
                                        return false;
                                    } else {
                                        $('#selected_date').val(start);
                                        document.getElementById("button_page").style.display = 'block';
                                    }
                                } else {
                                    $('#selected_date').val(start);
                                    document.getElementById("button_page").style.display = 'block';
                                }

                            } else {
                                $('#calendar').fullCalendar('unselect');
                                alert('Quota kosong atau habis pada tanggal tersebut');
                                return false;
                            }
                        } else {
                            $('#calendar').fullCalendar('unselect');
                            return false;
                        }
                        // $("td[data-date=" + start + "]").addClass("fc-highlight");
                        $(".fc-highlight").removeClass("fc-highlight");
                        // $(".fc-highlight").css("background-color", "green");
                        $("td[data-date=" + start + "]").addClass("fc-highlight");
                        // $("td[data-date=" + start + "]").css("background-color", "blue");
                    });
                },
                eventClick: function (calEvent, jsEvent, view) {
                    // console.log(this.date_selected);

                }
            });
        //    calendar.fullCalendar('render');
            // $('.test li a').click(function(){
            //     $('#calendar').fullCalendar('today');
            // });
        //    $('#tabs').tabs({
        //        show: function(event, ui) {
        //            $('#calendar').fullCalendar('render');
        //        }
        //    });
            // calendersasdas.fullCalendar('render');
        });

//        $(document).ready(function () {
//            $('.radio').click(function () {
//                document.getElementById("quota_avail").innerHTML = '';
//                document.getElementById("participant").innerHTML = '';
//                document.getElementById("pilihWaktu").innerHTML = '';
//                document.getElementById("button_page").style.display = 'none';
//                var waktu = $(this).val();
//                var tanggal_selected = $('#selected_date').val();
//                var newdate = new Date(tanggal_selected)
//                var token = "<%= req.session.token %>";
//                // var quota_selected = $('#quota_selected').val();
//                // console.log(tanggal_selected);
//                // console.log(waktu);
//                if (waktu == '') {
//                    // bootbox.alert("Harap pilih Waktunya");
//                    alert('Harap pilih Waktunya');
//                    return false;
//                }
//                if (tanggal_selected == '') {
//                    // bootbox.alert("Harap pilih Tanggal");
//                    alert('Harap pilih Tanggal');
//                    return false;
//                }
//                $.post("/datamaster/getQuotaByDateTime", {tanggal_selected: tanggal_selected, waktu: waktu}, function (obj) {
//                    if (obj.STATUS == 'SUKSES') {
//                        document.getElementById("quota_avail").innerHTML = obj.AVAILABILITY + ' Orang';
//                        document.getElementById("participant").innerHTML = obj.PARTICIPANT + ' Permohonan';
//                        document.getElementById("pilihWaktu").innerHTML = 'di ' + waktu + ' hari';
//                        if (obj.AVAILABILITY > 0) {
//                            document.getElementById("button_page").style.display = 'block';
//                        } else {
//                            document.getElementById("button_page").style.display = 'none';
//                        }
//                    } else {
//                        document.getElementById("quota_avail").innerHTML = obj.AVAILABILITY + ' Orang';
//                        document.getElementById("participant").innerHTML = obj.PARTICIPANT + ' Permohonan';
//                        alert(obj.MESSAGE);
//                        return false;
//                    }
//                });
//            });
//        });
    </script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
    <script src="<?php echo base_url(); ?>/assets/js/jquery.validate.min.js"></script>
    <script language="javascript">
            $(document).ready(function () {
                $('#merk').on('change', function () {
                    var merk_id = this.value;
                    $.ajax({
                        url: "<?= base_url('book/getTypeCarByBrand') ?>",
                        type: "POST",
                        data: {
                            merk_id: merk_id
                        },
                        cache: false,
                        success: function (result) {
                            $("#type").html(result);
                        }
                    });
                });

                $('#type').on('change', function () {
                    var type_id = this.value;
                    $.ajax({
                        url: "<?= base_url('book/getSizeByType') ?>",
                        type: "POST",
                        data: {
                            type_id: type_id
                        },
                        cache: false,
                        success: function (result) {
                            $("#size").val(result);
                        }
                    });
                });
            });

            $(document).ready(function () {
                $('#selectCompany').on('change', function () {
                    $('#dataPriceService').html('');
                    var company_id = this.value;
                    $.ajax({
                        url: "<?= base_url('book/getServiceByCompany') ?>",
                        type: "POST",
                        data: {
                            company_id: company_id
                        },
                        cache: false,
                        success: function (result) {
                            $("#selectService").html(result);
                        }
                    });
                });

                $('#selectService').on('change', function () {
                    var size_id = $('#size').val();
                    var company_id = $('#selectCompany').val();
                    var service_id = this.value;
                    if ($('#type').val() == '') {
                        alert('Silahkan pilih Jenis dan Type Mobil Terlebih Dahulu');
                        document.getElementById("jenis_pekerjaan").value = "";
                    } else {
                        $.ajax({
                            url: "<?= base_url('book/getPrice') ?>",
                            type: "POST",
                            data: {
                                service_id: service_id,
                                size_id: size_id,
                                company_id: company_id
                            },
                            cache: false,
                            success: function (result) {
                                data = JSON.parse(result);
                                console.log(data);
                                var status = data.status;
                                if (status == 'success') {
                                    $("#price_id").val(data.price_id);
                                    $("#price").val(data.price);
                                    $("#dp").val(data.dp);
                                    $('#dataPriceService').html(data.tmp);
                                } else {
                                    $("#price_id").val('');
                                    $("#price").val(data.message);
                                    $('#dataPriceService').html(data.tmp);
                                }
                            }
                        });
                    }
                });
            });
    </script>
    
</html>
