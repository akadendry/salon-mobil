<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Dashboard');
        if ($this->session->userdata('id') == NULL) {
            return redirect('login');
        }
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {
        $idUser = $this->session->userdata('id');
        $this->data['subMenu'] = 'notYet';
        $this->data['dataBooking'] = $this->M_Dashboard->getDataBookingCurrentByIdUser($idUser);
        if (count($this->data['dataBooking']) > 0) {
            foreach ($this->data['dataBooking'] as $row):
                $company_id = $row->company_id;
            endforeach;
            $this->data['rekening'] = $this->M_Dashboard->getAllRekeningByCompany($company_id);
        }else {
            $this->data['rekening'] = array();
        }

        $this->load->view('dashboard', $this->data);
    }

    public function waitApproval() {
        $idUser = $this->session->userdata('id');
        $this->data['subMenu'] = 'waitingApproval';
        $this->data['dataBooking'] = $this->M_Dashboard->getDataBookingCurrentWaitApprovalByIdUser($idUser);
        $this->load->view('dashboardWaitApproval', $this->data);
    }

    public function approval() {
        $idUser = $this->session->userdata('id');
        $this->data['subMenu'] = 'approval';
        $this->data['dataBooking'] = $this->M_Dashboard->getDataBookingCurrentApprovalByIdUser($idUser);
        $this->load->view('dashboardApproval', $this->data);
    }

    public function paymentMethod() {
        $idBooking = $this->uri->segment(3);
        $dataBooking = $this->M_Dashboard->getDataBookingById($idBooking);
        if (count($dataBooking) > 0) {
            foreach ($dataBooking as $row) {
                $company_id = $row->company_id;
                $get_data_rekening = $this->M_Dashboard->getAllRekeningByCompany($company_id);
                $data = array(
                    'expireDate' => $row->expireDateDp,
                    'dp' => $row->dp,
                    'company_id' => $company_id,
                    'rekening' => $get_data_rekening
                );
            }
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('book');
        }
        $this->load->view('paymentMethod', $data);
    }

    public function uploadBuktiPembayaran() {
        $id_booking = $this->input->post('id_booking');
        $config['upload_path'] = './assets/images/buktiPembayaran';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 10500;
        $config['encrypt_name'] = TRUE;
        $config['quality'] = 50;
        $this->load->library('upload', $config);
//        echo base_url().'assets/images/buktiPembayaran';
//        exit();
        if (!$this->upload->do_upload('imageDp')) {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', $this->upload->display_errors());
            redirect('dashboard');
        } else {
            $result1 = $this->upload->data();
            $imageDp = $result1['file_name'];
            $data = array(
                'statusDp' => 2,
                'imageDp' => $imageDp
            );
            $update = $this->M_Dashboard->updateDataBooking($data, $id_booking);
            if ($update == 1) {
                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('message', 'Upload Bukti Pembayaran Berhasil');
            } else {
                $this->session->set_flashdata('status', 'danger');
                $this->session->set_flashdata('message', 'Upload Bukti Pembayaran Gagal');
            }
            return redirect('dashboard');
        }
    }

    public function uploadBuktiPembayaranWait() {
        $id_booking = $this->input->post('id_booking');
        $config['upload_path'] = './assets/images/buktiPembayaran';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 10500;
        $config['encrypt_name'] = TRUE;
        $config['quality'] = 50;
        $this->load->library('upload', $config);
//        echo base_url().'assets/images/buktiPembayaran';
//        exit();
        if (!$this->upload->do_upload('imageDp')) {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', $this->upload->display_errors());
            redirect('dashbaord/waitApproval');
        } else {
            $result1 = $this->upload->data();
            $imageDp = $result1['file_name'];
            $data = array(
                'statusDp' => 2,
                'imageDp' => $imageDp
            );
            $update = $this->M_Dashboard->updateDataBooking($data, $id_booking);
            if ($update == 1) {
                $this->session->set_flashdata('status', 'success');
                $this->session->set_flashdata('message', 'Upload Bukti Pembayaran Berhasil');
            } else {
                $this->session->set_flashdata('status', 'danger');
                $this->session->set_flashdata('message', 'Upload Bukti Pembayaran gagal');
            }
            return redirect('dashboard/waitApproval');
        }
    }

    public function precheck() {
        $idUser = $this->session->userdata('id');
        $this->data['list_precheck'] = $this->M_Dashboard->getListPrecheckByIdUser($idUser);
        $this->data['subMenu'] = 'precheck';
        $this->load->view('list_precheck', $this->data);
    }

    public function detailPrecheck() {
        $idUser = $this->session->userdata('id');
        $this->data['subMenu'] = 'precheck';
        $id_precheck = $this->uri->segment(3);
        $this->data['idMenuBack'] = $this->uri->segment(4);
        $this->data['data_precheck'] = $this->M_Dashboard->get_data_precheck_by_id($id_precheck, $idUser);
        $this->data['list_service'] = $this->M_Dashboard->getAllDataService();
        $this->data['list_brand'] = $this->M_Dashboard->getAllDataBrand();
        $this->data['list_type'] = $this->M_Dashboard->getAllDataType();

        foreach ($this->data['data_precheck'] as $row):
            $this->data['tampak_satu'] = $row->tampak_satu;
            $this->data['tampak_dua'] = $row->tampak_dua;
            $this->data['tampak_tiga'] = $row->tampak_tiga;
            $this->data['tampak_empat'] = $row->tampak_empat;
        endforeach;
        $this->data['action'] = 'approve';
        $this->load->view('precheck_detail', $this->data);
    }

    public function proses() {
        $idUser = $this->session->userdata('id');
        $this->data['dataProses'] = $this->M_Dashboard->get_all_data_status_proses($idUser);
        $this->data['subMenu'] = 'proses';
        $this->load->view('proses', $this->data);
    }
    
    public function selesai(){
        $idUser = $this->session->userdata('id');
        $this->data['dataSelesai'] = $this->M_Dashboard->get_all_data_status_selesai($idUser);
        $this->data['subMenu'] = 'selesai';
        $this->load->view('selesai', $this->data);
    }
    
    public function prosesDetail() {
        $idUser = $this->session->userdata('id');
        $this->data['subMenu'] = 'selesai';
        $id_precheck = $this->uri->segment(3);
        $this->data['data_precheck'] = $this->M_Dashboard->get_data_precheck_by_id($id_precheck, $idUser);
        $this->data['action'] = 'detail';
        $this->load->view('proses_detail', $this->data);
    }
    
    public function history() {
        $idUser = $this->session->userdata('id');
        $this->data['list_precheck'] = $this->M_Dashboard->get_list_all_data_precheck($idUser);
        $this->data['subMenu'] = 'history';
        $this->data['cari'] = 0;
        $this->data['idMenuBack'] = $this->uri->segment(4);
        $this->load->view('list_all_data_precheck', $this->data);
    }
    
    public function seePrecheck() {
        $idUser = $this->session->userdata('id');
        $this->data['subMenu'] = 'history';
        $id_precheck = $this->uri->segment(3);
        $this->data['idMenuBack'] = $this->uri->segment(4);
        $this->data['data_precheck'] = $this->M_Dashboard->get_data_precheck_by_id($id_precheck, $idUser);
        foreach ($this->data['data_precheck'] as $row):
            $this->data['tampak_satu'] = $row->tampak_satu;
            $this->data['tampak_dua'] = $row->tampak_dua;
            $this->data['tampak_tiga'] = $row->tampak_tiga;
            $this->data['tampak_empat'] = $row->tampak_empat;
        endforeach;
        $this->data['action'] = 'detail';
        $this->load->view('precheck_detail', $this->data);
    }
    
    public function searchDataPrecheck() {
        $idUser = $this->session->userdata('id');
        if ($this->input->post('precheckDate') == '') {
            $precheckDate = '';
            $tanggalDipillih = '';
        } else {
            $precheckDate = date('Y-m-d', strtotime($this->input->post('precheckDate')));
            $tanggalDipillih = date('d-m-Y', strtotime($this->input->post('precheckDate')));
        }
        $status = $this->input->post('status');
        $this->data['list_precheck'] = $this->M_Dashboard->get_list_all_data_precheck_byDatePrecheck($precheckDate, $status, $idUser);
        $this->data['subMenu'] = 'history';
        $this->data['precheckDate'] = $tanggalDipillih;
        $this->data['status'] = $this->input->post('status');
        $this->data['cari'] = 1;
        $this->data['idMenuBack'] = $this->uri->segment(4);
        $this->load->view('list_all_data_precheck', $this->data);
    }

}
