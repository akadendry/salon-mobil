<?php

class M_Dashboard extends CI_Model {

    public function getDataBookingById($idBooking) {
        $data = $this->db->query("SELECT *
                FROM booking_temp
                WHERE id_booking is not null AND statusDp = 0 AND id_booking = '$idBooking'");
        return $data->result();
    }

    public function getDataBookingCurrentByIdUser($idUser) {
        $data = $this->db->query("SELECT a.*, b.name_service as namaService, c.name as nama_merk, d.name as nama_tipe
                FROM booking_temp a 
                LEFT JOIN mst_service as b ON a.name_service = b.id_service
                LEFT JOIN mst_brand as c ON a.car_brand = c.id
                LEFT JOIN mst_brand_type as d ON a.car_type = d.id
                WHERE id_booking is not null AND statusDp = 0 AND id_user = '$idUser' AND a.expireDateDp >= NOW()
                ORDER BY a.id_booking DESC");
//        echo $this->db->last_query();
//        exit();
        return $data->result();
    }

    public function getDataBookingCurrentWaitApprovalByIdUser($idUser) {
        $data = $this->db->query("SELECT a.*, b.name_service as namaService, c.name as nama_merk, d.name as nama_tipe
                FROM booking_temp a 
                LEFT JOIN mst_service as b ON a.name_service = b.id_service
                LEFT JOIN mst_brand as c ON a.car_brand = c.id
                LEFT JOIN mst_brand_type as d ON a.car_type = d.id
                WHERE id_booking is not null AND statusDp = 2 AND id_user = '$idUser' AND status_bayar = 0
                ORDER BY a.id_booking DESC");
//        $data = $this->db->query("SELECT a.*, b.name_service as namaService
//                FROM booking_temp a 
//                LEFT JOIN mst_service as b ON a.name_service = b.id_service
//                WHERE id_booking is not null AND statusDp = 2 AND id_user = '$idUser' AND DATE(a.expireDateDp) = CURDATE()
//                ORDER BY a.id_booking DESC");
        return $data->result();
    }

    public function getDataBookingCurrentApprovalByIdUser($idUser) {
        $data = $this->db->query("SELECT a.*, b.name_service as namaService, c.name as nama_merk, d.name as nama_tipe
                FROM booking_temp a 
                LEFT JOIN mst_service as b ON a.name_service = b.id_service
                LEFT JOIN mst_brand as c ON a.car_brand = c.id
                LEFT JOIN mst_brand_type as d ON a.car_type = d.id
                WHERE id_booking is not null AND statusDp = 1 AND id_user = '$idUser' AND status_bayar = 1
                ORDER BY a.id_booking DESC");
        return $data->result();
    }

    public function updateDataBooking($data, $id_booking) {
        $this->db->where('id_booking', $id_booking);
        $query = $this->db->update('booking_temp', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getAllRekeningByCompany($company_id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_rekening 
                                  WHERE deleted_at is null AND company_id = $company_id
                                  ORDER BY id DESC");
        return $data->result();
    }

    public function getListPrecheckByIdUser($user_id) {
        $data = $this->db->query("SELECT a.*, b.book_date, b.status_inspect, e.name as brand_name, f.name as car_type_name , c.name as nama_customer, c.email, c.phone, d.name_service
		FROM precheck a 
		LEFT JOIN booking_temp b ON a.id_booking = b.id_booking
		LEFT JOIN mst_users c ON b.id_user = c.id_user
                LEFT JOIN mst_service d ON b.name_service = d.id_service
                LEFT JOIN mst_brand e ON b.car_brand = e.id
                LEFT JOIN mst_brand_type f ON b.car_type = f.id
		WHERE b.status_bayar = 1 AND a.status = 0  AND DATE(b.book_date) = CURDATE() AND b.id_user = $user_id");
        return $data->result();
    }

    public function get_data_precheck_by_id($id_precheck, $idUser) {
        $data = $this->db->query("SELECT a.*, d.name_service as nama_servis, b.name, b.email, c.*, c.name_service as service_id, d.name_service, e.name_size, f.name as merk_mobil, g.name as type_mobil
                FROM precheck a 
                LEFT JOIN booking_temp c ON a.id_booking = c.id_booking 
                LEFT JOIN mst_users b ON c.id_user = b.id_user 
                LEFT JOIN mst_service d ON c.name_service = d.id_service
                LEFT JOIN car_size e ON c.id_size = e.id_size
                LEFT JOIN mst_brand f ON f.id = c.car_brand
                LEFT JOIN mst_brand_type g ON g.id = c.car_type
                WHERE a.id = $id_precheck AND c.id_user = $idUser");
        return $data->result();
    }

    public function getAllDataService() {
        $data = $this->db->query("SELECT * 
                                  FROM mst_service  
                                  WHERE is_active = 1 AND deleted_at is null
                                  ORDER BY id_service DESC");
        return $data->result();
    }

    public function getAllDataBrand() {
        $data = $this->db->query("SELECT *
                                  FROM mst_brand 
                                  WHERE deleted_at is null
                                  ORDER BY name ASC");
        return $data->result();
    }

    public function getAllDataType() {
        $data = $this->db->query("SELECT a.*, b.name as brand_name, c.name_size
                                  FROM mst_brand_type a
                                  JOIN mst_brand b ON a.brand_id = b.id
                                  JOIN car_size c ON a.size_id = c.id_size
                                  WHERE a.deleted_at is null
                                  ORDER BY a.name ASC");
        return $data->result();
    }

    function get_all_data_status_proses($user_id) {
        $data = $this->db->query("SELECT g.name as nama_bay, a.*, b.email, b.name, b.phone, c.*, d.name_service as nama_servis, e.name as nama_brand, f.name as nama_tipe 
		FROM booking_temp a 
		JOIN mst_users b ON a.id_user = b.id_user
		JOIN precheck c ON a.id_booking = c.id_booking
                JOIN mst_service d ON a.name_service = d.id_service
                JOIN mst_brand e ON a.car_brand = e.id
                JOIN mst_brand_type f ON a.car_type= f.id
                LEFT JOIN mst_bay g ON g.id = c.tempat_pengerjaan
		WHERE a.status_bayar = 1 AND c.status = 1 AND a.id_user = $user_id");
        return $data->result();
    }

    function get_all_data_status_selesai($user_id) {
        $data = $this->db->query("SELECT g.name as nama_bay, a.*, b.email, b.name, b.phone, c.*, d.name_service as nama_servis, e.name as nama_merk, f.name as nama_tipe
		FROM booking_temp a 
		JOIN mst_users b ON a.id_user = b.id_user
		JOIN precheck c ON a.id_booking = c.id_booking
                LEFT JOIN mst_service d ON a.name_service = d.id_service
                LEFT JOIN mst_brand e ON e.id = a.car_brand
                LEFT JOIN mst_brand_type f ON f.id = a.car_type
                LEFT JOIN mst_bay g ON g.id = c.tempat_pengerjaan
		WHERE a.status_bayar = 1 AND c.status = 2 AND a.id_user = $user_id");
        return $data->result();
    }

    public function get_list_all_data_precheck($idUser) {
        $data = $this->db->query("SELECT DISTINCT a.*, 
                d.name_service as nama_servis, b.car_brand, b.car_type, b.name_service, b.car_color, b.book_date, 
                c.name as nama_customer, c.email, c.phone, e.name as brand_name, 
                f.name as car_type_name, g.name as nama_lokasi
		FROM precheck a 
		LEFT JOIN booking_temp b ON a.id_booking = b.id_booking
		LEFT JOIN mst_users c ON b.id_user = c.id_user
                LEFT JOIN mst_service d ON b.name_service = d.id_service
                LEFT JOIN mst_brand e ON b.car_brand = e.id
                LEFT JOIN mst_brand_type f ON b.car_type = f.id
                LEFT JOIN mst_company g ON b.company_id = g.id
		WHERE b.status_bayar = 1 AND b.id_user = $idUser ORDER BY a.created_at DESC");
//		WHERE b.status_bayar = 1 ORDER BY b.book_date, a.status ASC");
        return $data->result();
    }
    
    public function get_list_all_data_precheck_byDatePrecheck($precheckDate, $status, $idUser) {
        if ($precheckDate == '') {
            $kondisiPrecheckDate = '';
        } else {
            $kondisiPrecheckDate = "AND b.book_date = '$precheckDate'";
        }
        if ($status == '') {
            $kondisiStatus = '';
        } elseif ($status == 3) {
            $kondisiStatus = "AND a.status = 0 AND DATE(b.book_date) > CURDATE() ";
        } else {
            $kondisiStatus = "AND a.status = '$status'";
        }
        $data = $this->db->query("SELECT DISTINCT a.*, 
                d.name_service as nama_servis, b.car_brand, b.car_type, b.name_service, b.car_color, b.book_date, 
                c.name as nama_customer, c.email, c.phone, e.name as brand_name, 
                f.name as car_type_name, g.name as nama_lokasi
		FROM precheck a 
		LEFT JOIN booking_temp b ON a.id_booking = b.id_booking
		LEFT JOIN mst_users c ON b.id_user = c.id_user
                LEFT JOIN mst_service d ON b.name_service = d.id_service
                LEFT JOIN mst_brand e ON b.car_brand = e.id
                LEFT JOIN mst_brand_type f ON b.car_type = f.id
                LEFT JOIN mst_company g ON b.company_id = g.id
		WHERE b.id_user = $idUser AND b.status_bayar = 1 $kondisiPrecheckDate $kondisiStatus");
        return $data->result();
    }

}
