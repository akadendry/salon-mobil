<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Selesai</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/jquery-ui.custom.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/chosen.min.css') ?>" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/fonts.googleapis.com.css') ?>" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/aceAdmin/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/ace-rtl.min.css') ?>" />


        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/aceAdmin/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/aceAdmin/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/aceAdmin/html5shiv.min.js"></script>
        <script src="assets/js/aceAdmin/respond.min.js"></script>
        <![endif]-->
        <style>
            select {
                -webkit-appearance: none;
                /*webkit browsers */
                -moz-appearance: none;
                /*Firefox */
                appearance: none;
                /* modern browsers */
                border-radius: 0;

            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            #imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

            /*#imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 :hover {opacity: 0.7;}*/

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            .modal-content-inovoice {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
            }

            /* Caption of Modal Image */
            #caption, #caption2, #caption3, #caption4, #caption5 {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation */
            .modal-content, #caption, #caption2, #caption3, #caption4, #caption5 {  
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {-webkit-transform:scale(0)} 
                to {-webkit-transform:scale(1)}
            }

            @keyframes zoom {
                from {transform:scale(0)} 
                to {transform:scale(1)}
            }

            /* The Close Button */
            .close, .close2, .close3, .close4, .close5 {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

            .close, .close2, .close3, .close4, .close5:hover,
            .close, .close2, .close3, .close4, .close5:focus {
                color: #bbb;
                text-decoration: none;
                cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px){
                .modal-content {
                    width: 100%;
                }
            }
        </style>
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                    <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/changePassword') ?>">
                                        <i class="ace-icon fa fa-lock"></i>
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <form method="post" action="<?= base_url('precheck/approved') ?>" class="form-horizontal" id="sample-form" enctype="multipart/form-data">
                    <?php foreach ($data_precheck as $row) : ?>
                        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                            <ul class="breadcrumb">
                                <li>
                                    <i class="ace-icon fa fa-home home-icon"></i>
                                    <a href="#">Home</a>
                                </li>

                                <li>
                                    <a href="<?= base_url('precheck') ?>">Precheck</a>
                                </li>

                                <li>
                                    <a href="#">Precheck Detail</a>
                                </li>
                            </ul><!-- /.breadcrumb -->
                        </div>

                        <div class="page-content">
                            <?php if ($this->session->flashdata('message')) : ?>
                                <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            <?php endif; ?>
                            <div class="page-header" style="text-align: center;">
                                <h1 style="color: black;">
                                    <b>PRE CHECK INSEPCTION</b>
                                </h1>
                            </div>
                            <div class="widget-box">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div id="fuelux-wizard-container">
                                            <div class="step-content pos-rel">
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Nama Customer</label>
                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <?php $name_full = $row->name . '(' . $row->email . ')'; ?>
                                                            <input type="text" name="nama_customer" class="width-100" readonly value="<?= $name_full; ?>" />
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">No Polisi</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" id="no_polisi" name="no_polisi" class="width-100" value="<?= $row->no_polisi ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Merk/Type</label>

                                                    <div class="col-sm-2">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="merk" class="width-100" value="<?= $row->merk_mobil ?>" readonly />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="type" class="width-100" value="<?= $row->type_mobil ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Ukuran Mobil</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="ukuran_mobil" class="width-100" value="<?= $row->name_size ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Warna Mobil</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="jenis_pekerjaan" class="width-100" value="<?= $row->car_color ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Jenis Pekerjaan</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="jenis_pekerjaan" class="width-100" value="<?= $row->nama_servis ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Price</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="jenis_pekerjaan" class="width-100" value="Rp. <?= number_format($row->price) ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.widget-main -->
                                </div><!-- /.widget-body -->
                            </div>
                        </div>

                        <div class="page-content">

                            <div class="page-header" style="text-align: center;">
                                <h1 style="color: black;">
                                    <b>KELENGKAPAN</b>
                                </h1>
                            </div><!-- /.page-header -->
                            <div class="widget-box">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div id="fuelux-wizard-container">
                                            <div class="step-content pos-rel">
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">&nbsp;</label>

                                                    <div class="col-sm-2" style="text-align: center;">
                                                        <b>Masuk</b>
                                                    </div>
                                                    <div class="col-sm-2" style="text-align: center;">
                                                        <b>Keluar</b>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">&nbsp;</label>

                                                    <div class="col-sm-2" style="text-align: center;">
                                                        <b>Masuk</b>
                                                    </div>
                                                    <div class="col-sm-2" style="text-align: center;">
                                                        <b>Keluar</b>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Ban Cadangan</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ban_cadangan_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ban_cadangan_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ban_cadangan_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ban_cadangan_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ban_cadangan_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ban_cadangan_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ban_cadangan_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ban_cadangan_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Air Conditioning & Blower</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="airConditioning_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->airConditioning_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->airConditioning_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->airConditioning_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="airConditioning_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->airConditioning_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->airConditioning_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->airConditioning_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Baut Derek</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="bautDerek_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->bautDerek_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->bautDerek_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->bautDerek_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="bautDerek_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->bautDerek_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->bautDerek_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->bautDerek_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Antena</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="antena_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->antena_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->antena_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->antena_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="antena_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->antena_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->antena_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->antena_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Buku Service & Buku Perawatan</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="bukuService_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->bukuService_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->bukuService_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->bukuService_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="bukuService_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->bukuService_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->bukuService_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->bukuService_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Foldback Mirror Electric</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="floodback_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->floodback_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->floodback_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->floodback_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="floodback_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->floodback_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->floodback_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->floodback_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">PIN Radio</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="pinRadion_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->pinRadion_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->pinRadion_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->pinRadion_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="pinRadio_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->pinRadio_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->pinRadio_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->pinRadio_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Air Radiator</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="airRadiator_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->airRadiator_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->airRadiator_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->airRadiator_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="airRadiator_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->airRadiator_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->airRadiator_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->airRadiator_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Dongkrak</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="dongkrak_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->dongkrak_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->dongkrak_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->dongkrak_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="dongkrak_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->dongkrak_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->dongkrak_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->dongkrak_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Ketinggian Brake & Cluth Fluit</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ketinggianBrake_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ketinggianBrake_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ketinggianBrake_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ketinggianBrake_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ketinggianBrake_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ketinggianBrake_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ketinggianBrake_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ketinggianBrake_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Dop/Tutup Roda</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="dopTutupRoda_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->dopTutupRoda_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->dopTutupRoda_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->dopTutupRoda_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="dopTutupRoda_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->dopTutupRoda_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->dopTutupRoda_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->dopTutupRoda_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Ketinggian Engine Oil</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ketinggianEngine_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ketinggianEngine_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ketinggianEngine_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ketinggianEngine_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ketinggianEngine_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ketinggianEngine_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ketinggianEngine_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ketinggianEngine_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Tas P3K</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="tasP3k_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->tasP3k_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->tasP3k_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->tasP3k_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="tasP3k_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->tasP3k_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->tasP3k_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->tasP3k_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Ketinggian PowerSteering Fluit</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ketinggianPowerSteering_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ketinggianPowerSteering_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ketinggianPowerSteering_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ketinggianPowerSteering_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="ketinggianPowerSteering_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->ketinggianPowerSteering_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->ketinggianPowerSteering_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->ketinggianPowerSteering_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Dudukan Plat Nomor</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="dudukanPlat_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->dudukanPlat_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->dudukanPlat_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->dudukanPlat_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="dudukanPlat_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->dudukanPlat_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->dudukanPlat_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->dudukanPlat_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Lampu Instrumen Panel</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuInstrumen_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuInstrumen_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuInstrumen_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuInstrumen_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuInstrumen_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuInstrumen_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuInstrumen_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuInstrumen_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Jok Depan & Belakang</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="jokDepanBelakang_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->jokDepanBelakang_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->jokDepanBelakang_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->jokDepanBelakang_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="jokDepanBelakang_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->jokDepanBelakang_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->jokDepanBelakang_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->jokDepanBelakang_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Lampu Belok & Darurat</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuBelokDarurat_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuBelokDarurat_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuBelokDarurat_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuBelokDarurat_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuBelokDarurat_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuBelokDarurat_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuBelokDarurat_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuBelokDarurat_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Kaca Spion Luar Lt l/RH</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="kacaSpionLuar_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->kacaSpionLuar_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->kacaSpionLuar_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->kacaSpionLuar_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="kacaSpionLuar_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->kacaSpionLuar_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->kacaSpionLuar_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->kacaSpionLuar_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Lampu Dalam</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuDalam_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuDalam_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuDalam_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuDalam_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuDalam_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuDalam_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuDalam_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuDalam_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Kaca Spion Dalam</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="kacaSpionDalam_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->kacaSpionDalam_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->kacaSpionDalam_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->kacaSpionDalam_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="kacaSpionDalam_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->kacaSpionDalam_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->kacaSpionDalam_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->kacaSpionDalam_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Lampu Dalam & Kecil</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuDalamKecil_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuDalamKecil_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuDalamKecil_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuDalamKecil_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuDalamKecil_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuDalamKecil_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuDalamKecil_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuDalamKecil_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Kunci Remot & Kontak</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="kunciRemotKontak_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->kunciRemotKontak_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->kunciRemotKontak_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->kunciRemotKontak_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="kunciRemotKontak_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->kunciRemotKontak_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->kunciRemotKontak_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->kunciRemotKontak_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Lampu Kabut</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuKabut_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuKabut_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuKabut_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuKabut_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuKabut_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuKabut_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuKabut_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuKabut_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Penutup Kaca Belakang</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="penutupKacaBelakang_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->penutupKacaBelakang_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->penutupKacaBelakang_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->penutupKacaBelakang_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="penutupKacaBelakang_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->penutupKacaBelakang_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->penutupKacaBelakang_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->penutupKacaBelakang_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Lampu Rem Parkir & Mundur</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuRemParkirMundur_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuRemParkirMundur_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuRemParkirMundur_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuRemParkirMundur_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="lampuRemParkirMundur_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->lampuRemParkirMundur_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->lampuRemParkirMundur_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->lampuRemParkirMundur_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Penyulut Rokok</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="penyulutRoko_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->penyulutRoko_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->penyulutRoko_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->penyulutRoko_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="penyulutRoko_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->penyulutRoko_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->penyulutRoko_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->penyulutRoko_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Power Window</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="powerWindow_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->powerWindow_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->powerWindow_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->powerWindow_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="powerWindow_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->powerWindow_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->powerWindow_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->powerWindow_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Sayap Bumper Depan</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="sayapBumperDepan_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->sayapBumperDepan_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->sayapBumperDepan_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->sayapBumperDepan_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="sayapBumperDepan_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->sayapBumperDepan_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->sayapBumperDepan_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->sayapBumperDepan_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Radio/Tape</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="radioTape_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->radioTape_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->radioTape_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->radioTape_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="radioTape_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->radioTape_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->radioTape_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->radioTape_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Segitiga Pengaman</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="segitigaPengaman_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->segitigaPengaman_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->segitigaPengaman_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->segitigaPengaman_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="segitigaPengaman_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->segitigaPengaman_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->segitigaPengaman_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->segitigaPengaman_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">CD Player & Magazine</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="cdPlayerMagazine_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->cdPlayerMagazine_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->cdPlayerMagazine_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->cdPlayerMagazine_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="cdPlayerMagazine_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->cdPlayerMagazine_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->cdPlayerMagazine_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->cdPlayerMagazine_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Sensor Parkir Mundur</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="sensorParkirMundur_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->sensorParkirMundur_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->sensorParkirMundur_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->sensorParkirMundur_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="sensorParkirMundur_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->sensorParkirMundur_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->sensorParkirMundur_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->sensorParkirMundur_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Central Lock</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="centralLock_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->centralLock_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->centralLock_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->centralLock_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="centralLock_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->centralLock_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->centralLock_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->centralLock_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Set Karpet</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="setKarpet_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->setKarpet_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->setKarpet_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->setKarpet_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="setKarpet_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->setKarpet_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->setKarpet_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->setKarpet_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Accu</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="accu_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->accu_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->accu_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->accu_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="accu_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->accu_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->accu_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->accu_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Payung</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="payung_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->payung_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->payung_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->payung_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="payung_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->payung_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->payung_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->payung_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Telephone</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="telephone_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->telephone_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->telephone_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->telephone_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="telephone_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->telephone_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->telephone_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->telephone_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">SNTK</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="stnk_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->stnk_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->stnk_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->stnk_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="stnk_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->stnk_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->stnk_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->stnk_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>

                                                    <label for="input" class="col-sm-2 no-padding-right">Wiper / Penyemprot Kaca</label>

                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="wiperPenyemprotKaca_masuk">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->wiperPenyemprotKaca_masuk == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->wiperPenyemprotKaca_masuk == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->wiperPenyemprotKaca_masuk == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" disabled name="wiperPenyemprotKaca_keluar">
                                                            <option value="">--Silahkan Pilih--</option>
                                                            <option value="x" <?php
                                                            if ($row->wiperPenyemprotKaca_keluar == 'x') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Tidak Ada</option>
                                                            <option value="v" <?php
                                                            if ($row->wiperPenyemprotKaca_keluar == 'v') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Berfungsi</option>
                                                            <option value="r" <?php
                                                            if ($row->wiperPenyemprotKaca_keluar == 'r') {
                                                                echo 'selected';
                                                            }
                                                            ?>>Ada & Tidak Berfungsi</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Lain-lain</label>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <textarea class="form-control" name="lain_lain" style="width: 100%; height: 100px;" readonly><?= $row->lain_lain ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    &nbsp;
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12" style="text-align: center">
                                                        <label for="input" class="col-sm-12 no-padding-right" style="font-size: 20px;"><b>Photo Exterior</b></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 1
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 3
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_satu != '') : ?>
                                                            <img id="imageTampak1"  src="https://admin.detailingshopgarage.com/<?= 'assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_satu ?>" id="tampak_satu" style="width:65%;height:200px;" />
                                                            <div id="caption"></div>
                                                        <?php else : ?>
                                                            <img src="https://admin.detailingshopgarage.com/<?= 'assets/images/NoImage.gif' ?>" id="tampak_satu" style="width:50%;height:200px;" />
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_tiga != '') : ?>
                                                            <img id="imageTampak3" src="https://admin.detailingshopgarage.com/<?= 'assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_tiga ?>" id="tampak_tiga" style="width:65%;height:200px;" />
                                                            <div id="caption3"></div>
                                                        <?php else : ?>
                                                            <img src="https://admin.detailingshopgarage.com/<?= 'assets/images/NoImage.gif' ?>" id="tampak_tiga" style="width:50%;height:200px;" />
                                                        <?php endif; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_satu" style="width: 100%; height: 100px;" readonly><?= $row->keterangan_tampak_satu ?></textarea>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_tiga" style="width: 100%; height: 100px;" readonly><?= $row->keterangan_tampak_tiga ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 2
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 4
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_dua != '') : ?>
                                                            <img id="imageTampak2" src="https://admin.detailingshopgarage.com/<?= 'assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_dua ?>" id="tampak_dua" style="width:65%;height:200px;" />
                                                            <div id="caption2"></div>
                                                        <?php else : ?>
                                                            <img src="https://admin.detailingshopgarage.com/<?= 'assets/images/NoImage.gif' ?>" id="tampak_dua" style="width:50%;height:200px;" />
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_empat != '') : ?>
                                                            <img id="imageTampak4" src="https://admin.detailingshopgarage.com/<?= 'assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_empat ?>" id="tampak_empat" style="width:65%;height:200px;" />
                                                            <div id="caption4"></div>
                                                        <?php else : ?>
                                                            <img src="https://admin.detailingshopgarage.com/<?= 'assets/images/NoImage.gif' ?>" id="tampak_empat" style="width:50%;height:200px;" />
                                                        <?php endif; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_dua" style="width: 100%; height: 100px;" readonly><?= $row->keterangan_tampak_dua ?></textarea>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_empat" style="width: 100%; height: 100px;" readonly><?= $row->keterangan_tampak_empat ?></textarea>
                                                    </div>
                                                </div>

                                                <!-- form tanda tangan-->
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_inspektor == null || $row->sign_inspektor == '') : ?>
                                                            <div id="signArea">
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Inspektor,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">

                                                                    <div class="typed"></div>
                                                                    <canvas class="sign-pad" id="sign-pad" width="300" height="100"></canvas>
                                                                    <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 2) : ?>
                                                                        <button id="btnSaveSign">Save Signature</button>
                                                                    <?php endif; ?>

                                                                </div>
                                                            </div>
                                                        <?php else : ?>
                                                            <div>
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Inspektor,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <img src="https://admin.detailingshopgarage.com/<?= 'assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_inspektor ?>" class="sign-preview" />
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>



                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_customer == null || $row->sign_customer == '') : ?>
                                                            <div id="signArea2">
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Customer,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">

                                                                    <div class="typed"></div>
                                                                    <canvas class="sign-pad2" id="sign-pad2" width="300" height="100"></canvas>
                                                                    <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 2) : ?>
                                                                        <button id="btnSaveSign2">Save Signature</button>
                                                                    <?php endif; ?>

                                                                </div>
                                                            </div>
                                                        <?php else : ?>
                                                            <div>
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Customer,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <img src="https://admin.detailingshopgarage.com/<?= 'assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_customer ?>" class="sign-preview2" />
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_admin == null || $row->sign_admin == '') : ?>
                                                            <div id="signArea3">
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Admin,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">

                                                                    <div class="typed"></div>
                                                                    <canvas class="sign-pad3" id="sign-pad3" width="300" height="100"></canvas>
                                                                    <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) : ?>
                                                                        <button id="btnSaveSign3">Save Signature</button>
                                                                    <?php endif; ?>

                                                                </div>
                                                            </div>
                                                        <?php else : ?>
                                                            <div>
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Admin,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <img src="https://admin.detailingshopgarage.com/<?= 'assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_admin ?>" class="sign-preview3" />
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="id_precheck" value="<?= $row->id; ?>" />
                                                <br>
                                                <?php if ($action === 'approve') : ?>

                                                    <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) : ?>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="text-align: center">
                                                                <input type="hidden" name="id_precheck" value="<?= $row->id; ?>" />
                                                                <input type="hidden" name="status" value="1" />

                                                                <button type="submit" class="btn btn-primary btn-block mt-3" style="border-radius: 8px;" onclick="return confirm('Setuju dengan data pre check nya?');"><span class="fa fa-check"></span> Approve</button>

                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 2) : ?>
                                                        <?php if ($row->sign_inspektor == null || $row->sign_customer == null) : ?>
                                                            <div class="form-group">
                                                                <div class="col-sm-12" style="text-align: center">
                                                                    <a href="<?= base_url('precheck/edit/' . $row->id) ?>" class="btn btn-success btn-block mt-3" style="border-radius: 8px;"><span class="fa fa-edit"></span> Final Inspek</a>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <?php if ($row->status == 2): ?>
                                                    <div class="form-group">
                                                        <div class="col-sm-12" style="text-align: center">
                                                            <?php if ($row->diskon == null || $row->diskon == ''): ?>
                                                                <a href="javascript:;" class="btn btn-success btn-block mt-3" style="border-radius: 8px;" id="buttonInvoice"> <span class="fa fa-money"></span> Bayar</a>
                                                            <?php else: ?>
                                                                <div class="col-sm-6" style="padding-left: 0px;">
                                                                    <a href="https://admin.detailingshopgarage.com/<?= 'assets/file/invoice/' . $row->invoice_dp ?>" class="btn btn-success btn-block mt-6" style="border-radius: 8px;" target="_blank"> <span class="fa fa-print"></span> Invoice DP</a>
                                                                </div>
                                                                <div class="col-sm-6" style="padding-right: 0px;">
                                                                    <a href="https://admin.detailingshopgarage.com/<?= 'assets/file/invoice/' . $row->invoice ?>" class="btn btn-success btn-block mt-6" style="border-radius: 8px;" target="_blank"> <span class="fa fa-print"></span> Invoice Pelunasan</a>
                                                                </div>
                                                                <!--<a href="</?= base_url('assets/file/invoice/' . $row->invoice) ?>" class="btn btn-success btn-block mt-3" style="border-radius: 8px;" target="_blank"> <span class="fa fa-print"></span> Invoice</a>-->
                                                                <!--<a href="</?= base_url('status/selesaiInvoice/'.$row->id.'/'.$row->id_booking)?>" class="btn btn-success btn-block mt-3" style="border-radius: 8px;"> <span class="fa fa-print"></span> Invoice</a>-->
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="form-group">
                                                    <div class="col-sm-12" style="text-align: center">
                                                        <a href="<?= base_url('dashboard/selesai') ?>" class="btn btn-danger btn-block mt-3" style="border-radius: 8px;"> <span class="fa fa-backward"></span> Kembali</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.widget-main -->
                                </div><!-- /.widget-body -->
                            </div>
                        </div>
                    <?php endforeach; ?>
                </form>
            </div><!-- /.main-content -->


            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!--untuk pop up gambar-->
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
        </div>
        <div id="myModal2" class="modal">
            <span class="close2">&times;</span>
            <img class="modal-content" id="img02">
            <div id="caption2"></div>
        </div>
        <div id="myModal3" class="modal">
            <span class="close3">&times;</span>
            <img class="modal-content" id="img03">
            <div id="caption3"></div>
        </div>
        <div id="myModal4" class="modal">
            <span class="close4">&times;</span>
            <img class="modal-content" id="img04">
            <div id="caption4"></div>
        </div>

        <div id="myModalInvoice" class="modal">
            <span class="close5">&times;</span>
            <div class="modal-content-inovoice">
                <form method="post" action="<?= base_url('status/cetakInvoice') ?>" class="form-horizontal" id="sample-form" enctype="multipart/form-data">
                    <?php foreach ($data_precheck as $rows) : ?>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Nama Customer</label>
                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <?php $name_fulls = $rows->name . '(' . $rows->email . ')'; ?>
                                        <input type="text" name="nama_customer" class="width-100" readonly value="<?= $name_fulls; ?>" />
                                        <input type="hidden" name="email_customer" class="width-100" readonly value="<?= $rows->email; ?>" />
                                    </span>
                                </div>
                                <div class="col-sm-6">&nbsp;</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">No Polisi</label>

                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" id="no_polisi" name="no_polisi" class="width-100" value="<?= $rows->no_polisi ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Merk/Type</label>

                                <div class="col-sm-2">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="merk" class="width-100" value="<?= $rows->merk_mobil ?>" readonly />
                                    </span>
                                </div>
                                <div class="col-sm-2">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="type" class="width-100" value="<?= $rows->type_mobil ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Ukuran Mobil</label>

                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="ukuran_mobil" class="width-100" value="<?= $rows->name_size ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Warna Mobil</label>

                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="jenis_pekerjaan" class="width-100" value="<?= $rows->car_color ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Jenis Pekerjaan</label>

                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="jenis_pekerjaan" class="width-100" value="<?= $rows->nama_servis ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Price</label>

                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="price" class="width-100" value="Rp. <?= number_format($rows->price) ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Pembayaran DP</label>

                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="dp" class="width-100" value="Rp. <?= number_format($rows->dp) ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="input" class="col-sm-2 no-padding-right">Sisa Pembayaran</label>
                                <?php
                                $sisa_bayar = (int) $rows->price - (int) $rows->dp;
                                ?>
                                <div class="col-sm-4">
                                    <span class="block input-icon input-icon-right">
                                        <input type="text" name="price" class="width-100" value="Rp. <?= number_format($sisa_bayar) ?>" readonly />
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="form-group">
                        <div class="row">
                            <label for="input" class="col-sm-2 no-padding-right">Diskon</label>

                            <div class="col-sm-2">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" id="diskon" name="diskon" class="width-100" value="0" maxlength="3" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                </span>
                            </div>
                            <div class="col-sm-2">%</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12" style="text-align: center">
                                <input type="hidden" name="id_precheck" value="<?= $rows->id; ?>" />
                                <input type="hidden" name="id_booking" value="<?= $rows->id_booking; ?>" />
                                <input type="hidden" name="id_size" value="<?= $rows->id_size; ?>" />
                                <input type="hidden" name="id_service" value="<?= $rows->service_id; ?>" />
                                <button type="submit" class="btn btn-primary btn-block mt-3" style="border-radius: 8px;" onclick="return confirm('Data sudah sesuai?');"><span class="fa fa-check"></span> Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--<div id="caption5"></div>-->
        </div>

        <script>
            var modal = document.getElementById("myModal");

            var img = document.getElementById("imageTampak1");
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function () {
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            var span = document.getElementsByClassName("close")[0];

            span.onclick = function () {
                modal.style.display = "none";
            }
        </script>
        <script>
            var modal2 = document.getElementById("myModal2");

            var img2 = document.getElementById("imageTampak2");
            var modalImg2 = document.getElementById("img02");
            var captionText2 = document.getElementById("caption2");
            img2.onclick = function () {
                modal2.style.display = "block";
                modalImg2.src = this.src;
                captionText2.innerHTML = this.alt;
            }

            var span2 = document.getElementsByClassName("close2")[0];

            span2.onclick = function () {
                modal2.style.display = "none";
            }
        </script>
        <script>
            var modal3 = document.getElementById("myModal3");

            var img3 = document.getElementById("imageTampak3");
            var modalImg3 = document.getElementById("img03");
            var captionText3 = document.getElementById("caption3");
            img3.onclick = function () {
                modal3.style.display = "block";
                modalImg3.src = this.src;
                captionText3.innerHTML = this.alt;
            }

            var span3 = document.getElementsByClassName("close3")[0];

            span3.onclick = function () {
                modal3.style.display = "none";
            }
        </script>
        <script>
            var modal4 = document.getElementById("myModal4");

            var img4 = document.getElementById("imageTampak4");
            var modalImg4 = document.getElementById("img04");
            var captionText4 = document.getElementById("caption4");
            img4.onclick = function () {
                modal4.style.display = "block";
                modalImg4.src = this.src;
                captionText4.innerHTML = this.alt;
            }

            var span4 = document.getElementsByClassName("close4")[0];

            span4.onclick = function () {
                modal4.style.display = "none";
            }
        </script>
        <script>
            var modalInvoice = document.getElementById("myModalInvoice");

            var buttonInvoice = document.getElementById("buttonInvoice");
//            var captionText5 = document.getElementById("caption5");

            buttonInvoice.onclick = function () {
                modalInvoice.style.display = "block";
//                captionText5.innerHTML = this.alt;
            }

            var spanInvoice = document.getElementsByClassName("close5")[0];

            spanInvoice.onclick = function () {
                modalInvoice.style.display = "none";
            }
        </script>

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="<?= base_url('assets/js/aceAdmin/jquery-2.1.4.min.js') ?>"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="assets/js/aceAdmin/jquery-1.11.3.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='assets/js/aceAdmin/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="<?= base_url('assets/js/aceAdmin/bootstrap.min.js') ?>"></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url('assets/js/aceAdmin/chosen.jquery.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/jquery.dataTables.bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/dataTables.buttons.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/buttons.flash.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/buttons.html5.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/buttons.print.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/buttons.colVis.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/dataTables.select.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/sign/numeric-1.2.6.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/sign/bezier.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/sign/jquery.signaturepad.js') ?>"></script>

        <script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
        <script src="./js/json2.min.js"></script>

        <!-- ace scripts -->
        <script src="<?= base_url('assets/js/aceAdmin/ace-elements.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/aceAdmin/ace.min.js') ?>"></script>
        <script>
            var id_precheck = document.getElementById("id_precheck").value;
            var no_polisi = document.getElementById("no_polisi").value;
            $(document).ready(function () {
                $('#signArea').signaturePad({
                    drawOnly: true,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });
            $(document).ready(function () {
                $('#signAreadisabled').signaturePad({
                    drawOnly: false,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });

            $("#btnSaveSign").click(function (e) {
                html2canvas([document.getElementById('sign-pad')], {
                    onrendered: function (canvas) {
                        var canvas_img_data = canvas.toDataURL('image/png');
                        var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                        // alert(no_polisi);
                        //ajax call to save image inside folder
                        $.ajax({
                            url: "<?= base_url('precheck/saveImageSignInspektor') ?>",
                            data: {
                                img_data: img_data,
                                id_precheck: id_precheck,
                                no_polisi: no_polisi
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
                                window.location.reload();
                            }
                        });
                    }
                });
            });

            $(document).ready(function () {
                $('#signArea2').signaturePad({
                    drawOnly: true,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });
            $(document).ready(function () {
                $('#signArea2disabled').signaturePad({
                    drawOnly: false,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });

            $("#btnSaveSign2").click(function (e) {
                html2canvas([document.getElementById('sign-pad2')], {
                    onrendered: function (canvas) {
                        var canvas_img_data = canvas.toDataURL('image/png');
                        var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                        // alert(no_polisi);
                        //ajax call to save image inside folder
                        $.ajax({
                            url: "<?= base_url('precheck/saveImageSignCustomer') ?>",
                            data: {
                                img_data: img_data,
                                id_precheck: id_precheck,
                                no_polisi: no_polisi
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
                                window.location.reload();
                            }
                        });
                    }
                });
            });

            $(document).ready(function () {
                $('#signArea3').signaturePad({
                    drawOnly: true,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });
            $(document).ready(function () {
                $('#signArea3disabled').signaturePad({
                    drawOnly: false,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });

            $("#btnSaveSign3").click(function (e) {
                html2canvas([document.getElementById('sign-pad3')], {
                    onrendered: function (canvas) {
                        var canvas_img_data = canvas.toDataURL('image/png');
                        var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                        // alert(no_polisi);
                        //ajax call to save image inside folder
                        $.ajax({
                            url: "<?= base_url('precheck/saveImageSignAdmin') ?>",
                            data: {
                                img_data: img_data,
                                id_precheck: id_precheck,
                                no_polisi: no_polisi
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </body>

</html>
