<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>History</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/jquery-ui.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/jquery-ui.custom.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/chosen.min.css') ?>" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/fonts.googleapis.com.css') ?>" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/aceAdmin/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/aceAdmin/ace-rtl.min.css') ?>" />


        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/aceAdmin/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/aceAdmin/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/aceAdmin/html5shiv.min.js"></script>
        <script src="assets/js/aceAdmin/respond.min.js"></script>
        <![endif]-->
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/changePassword') ?>">
                                        <i class="ace-icon fa fa-lock"></i>
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">Home</a>
                        </li>

                        <li>
                            <a href="#">Data Pre Check</a>
                        </li>
                    </ul><!-- /.breadcrumb -->
                </div>

                <div class="page-content">

                    <div class="page-header">
                        <h1>
                            Data Pre Check
                        </h1>
                    </div><!-- /.page-header -->
                    <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                    <div class="col-xs-12">
                                        <div style="float: right;">
                                            <a class="btn btn-lg btn-round btn-warning" id="searchButton" onchange="formFilter(this)">
                                                <i class="fa fa-search"></i> Search By
                                            </a>
                                        </div>
                                    </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12" style="margin-left: 13px;">
                                        <?php if ($cari == 0): ?>
                                            <?php
                                            $precheckDate = '';
                                            $status = '';
                                            ?>
                                            <div class="formSearch" style="display:none;">
                                            <?php else: ?>
                                                <?php
                                                $precheckDate = $precheckDate;
                                                $status = $status;
                                                ?>
                                                <div class="formSearch" style="display:block;">
                                                <?php endif; ?>
                                                <form class="form-group filter" action="<?php echo base_url('dashboard/searchDataPrecheck') ?>" method="post">
                                                    <div class="row" style="width: 100%;">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">          
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <label for="tipe" class="control-label">Pre Check Date</label>
                                                                    <div class="input-group date date_start">
                                                                        <input type="text" name="precheckDate" id="precheckDate" value="<?= $precheckDate ?>" class="form-control" />
                                                                        <span class="input-group-addon">
                                                                            <i class="ace-icon fa fa-calendar"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <label for="tipe" class="control-label">Status</label>
                                                                    <select class="form-control select2" name="status">
                                                                        <option value="" <?php if($status == ''){echo 'selected';}?>>Semua</option>                            
                                                                        <option value="0" <?php if($status == '0'){echo 'selected';}?>>Precheck</option>                            
                                                                        <option value="1" <?php if($status == '1'){echo 'selected';}?>>Proses</option>                            
                                                                        <option value="2" <?php if($status == '2'){echo 'selected';}?>>Selesai</option>                            
                                                                        <option value="3" <?php if($status == '3'){echo 'selected';}?>>Waiting For Precheck</option>                            
                                                                    </select>
                                                                </div>      
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <label for="tipe" class="control-label">&nbsp;</label>
                                                                    <div class="m-t-3">
                                                                        <input type="submit" class="btn btn-primary" value="cari" name="cari" style="width: 100%;">
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">&nbsp;</div>
                                    <div class="col-xs-12">
                                        <div class="table-header">
                                            &nbsp;
                                        </div>

                                        <!-- div.table-responsive -->

                                        <!-- div.dataTables_borderWrap -->

                                        <div style="overflow-x:auto;">
                                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name / Email / Phone</th>
                                                        <th>Car / Service Name</th>
                                                        <th>Lokasi Servis</th>
                                                        <th>Pre Check Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    <?php foreach ($list_precheck as $row): ?>
                                                        <tr>
                                                            <td><?= $no++ ?></td>
                                                            <td><?= $row->email . ' (' . $row->nama_customer . ')' . ' / ' . $row->phone ?></td>
                                                            <td><?= $row->brand_name . ' ' . $row->car_type_name . ' / ' . $row->nama_servis ?></td>
                                                            <td><?= $row->nama_lokasi ?></td>
                                                            <td><?= date('d-m-Y', strtotime($row->book_date)) ?></td>
                                                            <?php if ($row->status == 0): ?>
                                                                <?php if (strtotime(date('Y-m-d', strtotime($row->book_date))) > strtotime(date('Y-m-d'))): ?>
                                                                    <td>Waiting For Precheck</td>
                                                                    <td style="text-align: center;">&nbsp;</td>
                                                                <?php else: ?>
                                                                    <td>Precheck</td>
                                                                    <td style="text-align: center;"><a href="<?= base_url('dashboard/detailPrecheck/' . $row->id . '/3') ?>" style="cursor:pointer"><i class="fa fa-eye"> Pre Check Inspection </i></a></td>
                                                                <?php endif; ?>
                                                            <?php elseif ($row->status == 1): ?>
                                                                <td>Proses</td>
                                                                <td style="text-align: center;">Proses</td>
                                                                <!--<td style="text-align: center; color: blue;"><a href="</?= base_url('dashboard/seePrecheck/' . $row->id . '/3') ?>" style="cursor:pointer"><i class="fa fa-check"> Proses </i></a></td>-->
                                                            <?php elseif ($row->status == 2): ?>
                                                                <td>Selesai</td>
                                                                <td style="text-align: center; color: blue;"><a href="<?= base_url('dashboard/seePrecheck/' . $row->id . '/3') ?>" style="cursor:pointer"><i class="fa fa-check-square"> Selesai </i></a></td>
                                                            <?php endif; ?>

                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- PAGE CONTENT ENDS -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.page-content -->
                    </div>
                </div><!-- /.main-content -->


                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- basic scripts -->

            <!--[if !IE]> -->
            <script src="<?= base_url('assets/js/aceAdmin/jquery-2.1.4.min.js') ?>"></script>

            <!-- <![endif]-->

            <!--[if IE]>
        <script src="assets/js/aceAdmin/jquery-1.11.3.min.js"></script>
        <![endif]-->
            <script type="text/javascript">
                                            if ('ontouchstart' in document.documentElement)
                                                document.write("<script src='assets/js/aceAdmin/jquery.mobile.custom.min.js'>" + "<" + "/script>");
            </script>
            <script src="<?= base_url('assets/js/aceAdmin/bootstrap.min.js') ?>"></script>

            <!-- page specific plugin scripts -->
            <script src="<?= base_url('assets/js/aceAdmin/jquery-ui.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/chosen.jquery.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/jquery.dataTables.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/jquery.dataTables.bootstrap.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/dataTables.buttons.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/buttons.flash.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/buttons.html5.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/buttons.print.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/buttons.colVis.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/dataTables.select.min.js') ?>"></script>

            <!-- ace scripts -->
            <script src="<?= base_url('assets/js/aceAdmin/ace-elements.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/aceAdmin/ace.min.js') ?>"></script>

            <!-- inline scripts related to this page -->
            <script>
                                            $(document).ready(function () {
                                                $('#searchButton').click(function () {
                                                    $('.formSearch').slideToggle("slow");
                                                });
                                            });
            </script>
            <script type="text/javascript">
                jQuery(function ($) {
                    //initiate dataTables plugin
                    var myTable =
                            $('#dynamic-table')
                            //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                            .DataTable({
                                bAutoWidth: false,
                                "aoColumns": [
                                    {"bSortable": false},
                                    null, null, null, null, null,
                                    {"bSortable": false}
                                ],
                                "aaSorting": [],

                                //"bProcessing": true,
                                //"bServerSide": true,
                                //"sAjaxSource": "http://127.0.0.1/table.php"	,

                                //,
                                //"sScrollY": "200px",
                                //"bPaginate": false,

                                //"sScrollX": "100%",
                                //"sScrollXInner": "120%",
                                //"bScrollCollapse": true,
                                //Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
                                //you may want to wrap the table inside a "div.dataTables_borderWrap" element

                                //"iDisplayLength": 50


                                select: {
                                    style: 'multi'
                                }
                            });



                    $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                    new $.fn.dataTable.Buttons(myTable, {
                        buttons: [
                            {
                                "extend": "colvis",
                                "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                                "className": "btn btn-white btn-primary btn-bold",
                                columns: ':not(:first):not(:last)'
                            },
                            {
                                "extend": "copy",
                                "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "csv",
                                "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "excel",
                                "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "pdf",
                                "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "print",
                                "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                                "className": "btn btn-white btn-primary btn-bold",
                                autoPrint: false,
                                message: 'This print was produced using the Print button for DataTables'
                            }
                        ]
                    });
                    myTable.buttons().container().appendTo($('.tableTools-container'));

                    //style the message box
                    var defaultCopyAction = myTable.button(1).action();
                    myTable.button(1).action(function (e, dt, button, config) {
                        defaultCopyAction(e, dt, button, config);
                        $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
                    });


                    var defaultColvisAction = myTable.button(0).action();
                    myTable.button(0).action(function (e, dt, button, config) {

                        defaultColvisAction(e, dt, button, config);


                        if ($('.dt-button-collection > .dropdown-menu').length == 0) {
                            $('.dt-button-collection')
                                    .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
                                    .find('a').attr('href', '#').wrap("<li />")
                        }
                        $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
                    });

                    ////

                    setTimeout(function () {
                        $($('.tableTools-container')).find('a.dt-button').each(function () {
                            var div = $(this).find(' > div').first();
                            if (div.length == 1)
                                div.tooltip({container: 'body', title: div.parent().text()});
                            else
                                $(this).tooltip({container: 'body', title: $(this).text()});
                        });
                    }, 500);





                    myTable.on('select', function (e, dt, type, index) {
                        if (type === 'row') {
                            $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
                        }
                    });
                    myTable.on('deselect', function (e, dt, type, index) {
                        if (type === 'row') {
                            $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
                        }
                    });




                    /////////////////////////////////
                    //table checkboxes
                    $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

                    //select/deselect all rows according to table header checkbox
                    $('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function () {
                        var th_checked = this.checked;//checkbox inside "TH" table header

                        $('#dynamic-table').find('tbody > tr').each(function () {
                            var row = this;
                            if (th_checked)
                                myTable.row(row).select();
                            else
                                myTable.row(row).deselect();
                        });
                    });

                    //select/deselect a row when the checkbox is checked/unchecked
                    $('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
                        var row = $(this).closest('tr').get(0);
                        if (this.checked)
                            myTable.row(row).deselect();
                        else
                            myTable.row(row).select();
                    });



                    $(document).on('click', '#dynamic-table .dropdown-toggle', function (e) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        e.preventDefault();
                    });



                    //And for the first simple table, which doesn't have TableTools or dataTables
                    //select/deselect all rows according to table header checkbox
                    var active_class = 'active';
                    $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
                        var th_checked = this.checked;//checkbox inside "TH" table header

                        $(this).closest('table').find('tbody > tr').each(function () {
                            var row = this;
                            if (th_checked)
                                $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                            else
                                $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                        });
                    });

                    //select/deselect a row when the checkbox is checked/unchecked
                    $('#simple-table').on('click', 'td input[type=checkbox]', function () {
                        var $row = $(this).closest('tr');
                        if ($row.is('.detail-row '))
                            return;
                        if (this.checked)
                            $row.addClass(active_class);
                        else
                            $row.removeClass(active_class);
                    });



                    /********************************/
                    //add tooltip for small view action buttons in dropdown menu
                    $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

                    //tooltip placement on right or left
                    function tooltip_placement(context, source) {
                        var $source = $(source);
                        var $parent = $source.closest('table')
                        var off1 = $parent.offset();
                        var w1 = $parent.width();

                        var off2 = $source.offset();
                        //var w2 = $source.width();

                        if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                            return 'right';
                        return 'left';
                    }




                    /***************/
                    $('.show-details-btn').on('click', function (e) {
                        e.preventDefault();
                        $(this).closest('tr').next().toggleClass('open');
                        $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
                    });
                    /***************/





                    /**
                     //add horizontal scrollbars to a simple table
                     $('#simple-table').css({'width':'2000px', 'max-width': 'none'}).wrap('<div style="width: 1000px;" />').parent().ace_scroll(
                     {
                     horizontal: true,
                     styleClass: 'scroll-top scroll-dark scroll-visible',//show the scrollbars on top(default is bottom)
                     size: 2000,
                     mouseWheelLock: true
                     }
                     ).css('padding-top', '12px');
                     */


                })
            </script>
            <script type="text/javascript">
                jQuery(function ($) {

                    $("#precheckDate").datepicker({
                        showOtherMonths: true,
                        selectOtherMonths: false,
                        dateFormat: 'd-m-yy'
                    });
                });
            </script>
    </body>
</html>
