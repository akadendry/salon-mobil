<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>DSG - Book For Service</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>/assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/favicon.png" />

        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

        <!-- CSS Files -->
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap1.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>/assets/css/material-bootstrap-wizard1.css" rel="stylesheet" />

        <style>
            .fc-highlight {
                background: blue !important;
                opacity: inherit;
            }
        </style>

        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="<?php echo base_url(); ?>/assets/css/demo.css" rel="stylesheet" />
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body>
        <div class="image-container set-full-height" style="background-color:white;">
            <!--   Creative Tim Branding   -->
            <a href="<?php echo base_url(); ?>home">
                <div class="logo-container">
                    <div class="logo">
                        <img src="<?php echo base_url(); ?>/assets/images/logo_abu.png">
                    </div>

                </div>
            </a>

            <!--   Big container   -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <!--      Wizard container        -->
                        <div class="wizard-container">
                            <div class="card wizard-card" data-color="red" id="wizard">
                                <?php if ($this->session->flashdata('message')): ?>
                                    <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                                        <?php echo $this->session->flashdata('message'); ?>
                                    </div>
                                <?php endif; ?>
                                <form action="<?= base_url('Book/order') ?>" method="post">
                                    <div class="wizard-header">
                                        <h4 class="wizard-title">
                                            Segera Selesaikan Pembayaran DP nya Untuk Diproses Lebih Lanjut
                                        </h4>
                                    </div>
                                    <div class="wizard-content">
                                        <div style="text-align: center;">
                                            <h5 style="color: black;"><b>Batas Waktu Pembayaran</b></h5>
                                            <b><h5 id="countDown"></h5></b>
                                            <br>
                                        </div>
                                        <h4 class="wizard-title" style="padding-left: 20px;">
                                            Jumlah yang Harus Dibayar :
                                        </h4>
                                        <h5 class="wizard-title" style="padding-left: 20px; color: green;">
                                            Rp. <?= number_format($dp)?>
                                        </h5>
                                        <br>
                                        <h4 class="wizard-title" style="padding-left: 20px;">
                                            Pembayaran Dapat Melalui Bank :
                                        </h4>
                                        <div style="padding-left: 70px;">
                                            <table>
                                                <?php foreach($rekening as $rk):?>
                                                    <tr>
                                                        <td style="width: 25%; padding-right: 20px;"><img src="<?= 'https://admin.detailingshopgarage.com/assets/images/rekening/'.$company_id.'/'.$rk->image_bank ?>" style="width: 50%;"></td>
                                                        <td><h4><b><?= $rk->no ?> (<?= $rk->atas_nama?>)</b></h4></td>
                                                    </tr>
                                                <?php endforeach;?>
<!--                                                <tr>
                                                    <td style="width: 50%;"><img src="<?= base_url('assets/images/logo_bank/bca.jpg') ?>" style="width: 200px;"></td>
                                                    <td><h4><b>47599998887 (PT. Arqira Indo Pratama)</b></h4></td>
                                                </tr>-->
<!--                                                <tr>
                                                    <td><img src="</?= base_url('assets/images/logo_bank/bni.png') ?>" style="width: 200px;"></td>
                                                    <td><h4><b>987654321 (Atas Nama DSG Group)</b></h4></td>
                                                </tr>-->
                                            </table>

                                        </div>
                                        <div style="padding-left: 20px;">
                                            <h5 style="color: green;">Catatan : Jika Sudah Transfer Segera Upload Bukti Transfer</h5>
                                        </div>
                                    </div>
                                    <div class="wizard-footer">
                                        <a href="<?= base_url('dashboard')?>" class="btn btn-success" style="width: 100%;">Cek Status Pembayaran</a>
                                    </div>
                                </form>
                            </div>
                        </div> <!-- wizard container -->
                    </div>
                </div> <!-- row -->
            </div> <!--  big container -->

            <div class="footer">
                <div class="container text-center">
                    Copyright DSGINDONESIA 2019
                </div>
            </div>
        </div>

    </body>
    <!--   Core JS Files   -->
    <script src="<?php echo base_url(); ?>/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/assets/js/jquery.bootstrap.js" type="text/javascript"></script>

    <!--  Plugin for the Wizard -->
    <script src="<?php echo base_url(); ?>/assets/js/material-bootstrap-wizard.js"></script>

    <!--js untuk full calender-->
    <script src="<?php echo base_url(); ?>/assets/js/aceAdmin/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/aceAdmin/fullcalendar.min.js"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
    <script src="<?php echo base_url(); ?>/assets/js/jquery.validate.min.js"></script>
    <script>
            // Set the date we're counting down to
            // var countDownDate = "2019-11-02 08:00:00";
            var expireDateDp = "<?= $expireDate ?>";
            var countDownDate = new Date(expireDateDp).getTime();
            // console.log("<%= req.session.data.list_detail.FULL_DATETIME_FOR_COUNTDOWN %>");

            // Update the count down every 1 second
            var x = setInterval(function () {

                // Get today's date and time
                var now = new Date().getTime();

                // Find the distance between now and the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Output the result in an element with id="demo"
                document.getElementById("countDown").innerHTML = hours + " Jam : "
                        + minutes + " Menit : " + seconds + " Detik ";

                // If the count down is over, write some text 
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("countDown").innerHTML = "Sudah Lewat Masa Pembayarannya";
                }
            }, 500);
    </script>
</html>
