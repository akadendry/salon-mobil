<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Precheck</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/aceAdmin/bootstrap.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- page specific plugin styles -->

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/aceAdmin/fonts.googleapis.com.css" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/aceAdmin/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="<?= base_url() ?>assets/css/aceAdmin/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/aceAdmin/ace-skins.min.css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/aceAdmin/ace-rtl.min.css" />

        <style>
            .modal {
                display: none;
                /* Hidden by default */
                position: fixed;
                /* Stay in place */
                z-index: 1;
                /* Sit on top */
                padding-top: 200px;
                /* Location of the box */
                left: 0;
                top: 0;
                width: 100%;
                /* Full width */
                height: 100%;
                /* Full height */
                overflow: auto;
                /* Enable scroll if needed */
                background-color: rgb(0, 0, 0);
                /* Fallback color */
                background-color: rgba(0, 0, 0, 0.4);
                /* Black w/ opacity */
            }

            /* Modal Content */

            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
            }
        </style>
        <style>

            #imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

            /*#imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 :hover {opacity: 0.7;}*/

            /* The Modal (background) */
            .modal-pembayaranDp {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content-pembayaranDp {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            /* Caption of Modal Image */
            #caption, #caption2, #caption3, #caption4 {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation */
            .modal-content-pembayaranDp, #caption, #caption2, #caption3, #caption4 {  
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {-webkit-transform:scale(0)} 
                to {-webkit-transform:scale(1)}
            }

            @keyframes zoom {
                from {transform:scale(0)} 
                to {transform:scale(1)}
            }

            /* The Close Button */
            .closeDp, .close2, .close3, .close4 {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

            .closeDp, .close2, .close3, .close4:hover,
            .close, .close2, .close3, .close4:focus {
                color: #bbb;
                text-decoration: none;
                cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px){
                .modal-content-pembayaranDp {
                    width: 100%;
                }
            }
        </style>

        <!--[if lte IE 9]>
          <link rel="stylesheet" href="<?= base_url() ?>assets/css/aceAdmin/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url() ?>assets/js/aceAdmin/ace-extra.min.js"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="<?= base_url() ?>assets/js/aceAdmin/html5shiv.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/respond.min.js"></script>
        <![endif]-->
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="<?= base_url() ?>" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                    <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">Home</a>
                        </li>

                        <li>
                            <a href="#">Data Booking</a>
                        </li>
                    </ul><!-- /.breadcrumb -->
                </div>

                <div class="page-content">

                    <div class="page-header">
                        <h1>
                            Data Booking
                        </h1>
                    </div><!-- /.page-header -->
                    <?php if ($this->session->flashdata('message')) : ?>
                        <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-header">
                                        &nbsp;
                                    </div>

                                    <!-- div.table-responsive -->

                                    <!-- div.dataTables_borderWrap -->
                                    <div style="overflow-x:auto;">
                                        <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <!--<th>Name / Email / Phone</th>-->
                                                    <!--<th>Phone</th>-->
                                                    <th>Car</th>
                                                    <th>Service Name</th>
                                                    <th>Pre Check Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                    <th hidden>&nbsp;</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php $no = 1; ?>
                                                <?php foreach ($list_precheck as $row): ?>
                                                    <tr>
                                                        <td><?= $no++ ?></td>
                                                        <!--<td></?= $row->email . ' (' . $row->nama_customer . ')' . ' / ' . $row->phone ?></td>-->
    <!--                                                            <td></?= $row->phone ?></td>-->
                                                        <td><?= $row->brand_name . ' ' . $row->car_type_name ?></td>
                                                        <td><?= $row->name_service ?></td>
                                                        <td><?= date('d-m-Y', strtotime($row->book_date)) ?></td>
                                                        <?php if ($row->status == 0): ?>
                                                            <td>Pre Check</td>
                                                            <?php if($row->type_booking == 'datang'):?>
                                                            <td style="text-align: center;"><a href="<?= base_url('dashboard/detailPrecheck/' . $row->id.'/1') ?>" style="cursor:pointer"><i class="fa fa-eye"> Detail </i></a></td>
                                                            <?php else:?>
                                                                <?php if($row->status_inspect == 'belum'):?>
                                                                <td style="text-align: center;"><span><b>Sedang Pemeriksaan</b></span></td>
                                                                <?php else:?>
                                                                    <td style="text-align: center;"><a href="<?= base_url('dashboard/detailPrecheck/' . $row->id.'/1') ?>" style="cursor:pointer"><i class="fa fa-eye"> Detail </i></a></td>
                                                                <?php endif;?>
                                                            <?php endif;?>
                                                        <?php elseif ($row->status == 1): ?>
                                                            <td>Proses</td>
                                                            <td style="text-align: center; color: blue;"><a href="<?= base_url('dashboard/seePrecheck/' . $row->id.'/1') ?>" style="cursor:pointer"><i class="fa fa-check"> Detail </i></a></td>
                                                        <?php elseif ($row->status == 2): ?>
                                                            <td>Selesai</td>
                                                            <td style="text-align: center; color: blue;"><a href="<?= base_url('dashboard/seePrecheck/' . $row->id.'/1') ?>" style="cursor:pointer"><i class="fa fa-check-square"> Detail </i></a></td>
                                                        <?php endif; ?>
                                                            <td hidden>&nbsp;</td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div>
            </div><!-- /.main-content -->


            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <div id="myModalDp" class="modal-pembayaranDp">
            <span class="closeDp">&times;</span>
            <img class="modal-content-pembayaranDp" id="img01">
            <div id="caption"></div>
        </div>
        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="<?= base_url() ?>assets/js/aceAdmin/jquery-2.1.4.min.js"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="<?= base_url() ?>assets/js/aceAdmin/jquery-1.11.3.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
                                                                    if ('ontouchstart' in document.documentElement)
                                                                        document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url() ?>assets/js/aceAdmin/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/jquery.dataTables.bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/dataTables.buttons.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/buttons.flash.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/buttons.html5.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/buttons.print.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/buttons.colVis.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/dataTables.select.min.js"></script>

        <!-- ace scripts -->
        <script src="<?= base_url() ?>assets/js/aceAdmin/ace-elements.min.js"></script>
        <script src="<?= base_url() ?>assets/js/aceAdmin/ace.min.js"></script>
        <script type="text/javascript">
                                                                    jQuery(function ($) {
                                                                        //initiate dataTables plugin
                                                                        var myTable =
                                                                                $('#dynamic-table')
                                                                                //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                                                                                .DataTable({
                                                                                    bAutoWidth: false,
                                                                                    "aoColumns": [{
                                                                                            "bSortable": false
                                                                                        },
                                                                                        null, null, null, null, null,
                                                                                        {
                                                                                            "bSortable": false
                                                                                        }
                                                                                    ],
                                                                                    "aaSorting": [],

                                                                                    //"bProcessing": true,
                                                                                    //"bServerSide": true,
                                                                                    //"sAjaxSource": "http://127.0.0.1/table.php"	,

                                                                                    //,
                                                                                    //"sScrollY": "200px",
                                                                                    //"bPaginate": false,

                                                                                    //"sScrollX": "100%",
                                                                                    //"sScrollXInner": "120%",
                                                                                    //"bScrollCollapse": true,
                                                                                    //Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
                                                                                    //you may want to wrap the table inside a "div.dataTables_borderWrap" element

                                                                                    //"iDisplayLength": 50


                                                                                    select: {
                                                                                        style: 'multi'
                                                                                    }
                                                                                });



                                                                        $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                                                                        new $.fn.dataTable.Buttons(myTable, {
                                                                            buttons: [{
                                                                                    "extend": "colvis",
                                                                                    "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                                                                                    "className": "btn btn-white btn-primary btn-bold",
                                                                                    columns: ':not(:first):not(:last)'
                                                                                },
                                                                                {
                                                                                    "extend": "copy",
                                                                                    "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                                                                                    "className": "btn btn-white btn-primary btn-bold"
                                                                                },
                                                                                {
                                                                                    "extend": "csv",
                                                                                    "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                                                                                    "className": "btn btn-white btn-primary btn-bold"
                                                                                },
                                                                                {
                                                                                    "extend": "excel",
                                                                                    "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                                                                                    "className": "btn btn-white btn-primary btn-bold"
                                                                                },
                                                                                {
                                                                                    "extend": "pdf",
                                                                                    "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                                                                                    "className": "btn btn-white btn-primary btn-bold"
                                                                                },
                                                                                {
                                                                                    "extend": "print",
                                                                                    "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                                                                                    "className": "btn btn-white btn-primary btn-bold",
                                                                                    autoPrint: false,
                                                                                    message: 'This print was produced using the Print button for DataTables'
                                                                                }
                                                                            ]
                                                                        });
                                                                        myTable.buttons().container().appendTo($('.tableTools-container'));

                                                                        //style the message box
                                                                        var defaultCopyAction = myTable.button(1).action();
                                                                        myTable.button(1).action(function (e, dt, button, config) {
                                                                            defaultCopyAction(e, dt, button, config);
                                                                            $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
                                                                        });


                                                                        var defaultColvisAction = myTable.button(0).action();
                                                                        myTable.button(0).action(function (e, dt, button, config) {

                                                                            defaultColvisAction(e, dt, button, config);


                                                                            if ($('.dt-button-collection > .dropdown-menu').length == 0) {
                                                                                $('.dt-button-collection')
                                                                                        .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
                                                                                        .find('a').attr('href', '#').wrap("<li />")
                                                                            }
                                                                            $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
                                                                        });

                                                                        ////

                                                                        setTimeout(function () {
                                                                            $($('.tableTools-container')).find('a.dt-button').each(function () {
                                                                                var div = $(this).find(' > div').first();
                                                                                if (div.length == 1)
                                                                                    div.tooltip({
                                                                                        container: 'body',
                                                                                        title: div.parent().text()
                                                                                    });
                                                                                else
                                                                                    $(this).tooltip({
                                                                                        container: 'body',
                                                                                        title: $(this).text()
                                                                                    });
                                                                            });
                                                                        }, 500);





                                                                        myTable.on('select', function (e, dt, type, index) {
                                                                            if (type === 'row') {
                                                                                $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
                                                                            }
                                                                        });
                                                                        myTable.on('deselect', function (e, dt, type, index) {
                                                                            if (type === 'row') {
                                                                                $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
                                                                            }
                                                                        });




                                                                        /////////////////////////////////
                                                                        //table checkboxes
                                                                        $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

                                                                        //select/deselect all rows according to table header checkbox
                                                                        $('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function () {
                                                                            var th_checked = this.checked; //checkbox inside "TH" table header

                                                                            $('#dynamic-table').find('tbody > tr').each(function () {
                                                                                var row = this;
                                                                                if (th_checked)
                                                                                    myTable.row(row).select();
                                                                                else
                                                                                    myTable.row(row).deselect();
                                                                            });
                                                                        });

                                                                        //select/deselect a row when the checkbox is checked/unchecked
                                                                        $('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
                                                                            var row = $(this).closest('tr').get(0);
                                                                            if (this.checked)
                                                                                myTable.row(row).deselect();
                                                                            else
                                                                                myTable.row(row).select();
                                                                        });



                                                                        $(document).on('click', '#dynamic-table .dropdown-toggle', function (e) {
                                                                            e.stopImmediatePropagation();
                                                                            e.stopPropagation();
                                                                            e.preventDefault();
                                                                        });



                                                                        //And for the first simple table, which doesn't have TableTools or dataTables
                                                                        //select/deselect all rows according to table header checkbox
                                                                        var active_class = 'active';
                                                                        $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
                                                                            var th_checked = this.checked; //checkbox inside "TH" table header

                                                                            $(this).closest('table').find('tbody > tr').each(function () {
                                                                                var row = this;
                                                                                if (th_checked)
                                                                                    $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                                                                                else
                                                                                    $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                                                                            });
                                                                        });

                                                                        //select/deselect a row when the checkbox is checked/unchecked
                                                                        $('#simple-table').on('click', 'td input[type=checkbox]', function () {
                                                                            var $row = $(this).closest('tr');
                                                                            if ($row.is('.detail-row '))
                                                                                return;
                                                                            if (this.checked)
                                                                                $row.addClass(active_class);
                                                                            else
                                                                                $row.removeClass(active_class);
                                                                        });



                                                                        /********************************/
                                                                        //add tooltip for small view action buttons in dropdown menu
                                                                        $('[data-rel="tooltip"]').tooltip({
                                                                            placement: tooltip_placement
                                                                        });

                                                                        //tooltip placement on right or left
                                                                        function tooltip_placement(context, source) {
                                                                            var $source = $(source);
                                                                            var $parent = $source.closest('table')
                                                                            var off1 = $parent.offset();
                                                                            var w1 = $parent.width();

                                                                            var off2 = $source.offset();
                                                                            //var w2 = $source.width();

                                                                            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                                                                                return 'right';
                                                                            return 'left';
                                                                        }




                                                                        /***************/
                                                                        $('.show-details-btn').on('click', function (e) {
                                                                            e.preventDefault();
                                                                            $(this).closest('tr').next().toggleClass('open');
                                                                            $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
                                                                        });
                                                                        /***************/





                                                                        /**
                                                                         //add horizontal scrollbars to a simple table
                                                                         $('#simple-table').css({'width':'2000px', 'max-width': 'none'}).wrap('<div style="width: 1000px;" />').parent().ace_scroll(
                                                                         {
                                                                         horizontal: true,
                                                                         styleClass: 'scroll-top scroll-dark scroll-visible',//show the scrollbars on top(default is bottom)
                                                                         size: 2000,
                                                                         mouseWheelLock: true
                                                                         }
                                                                         ).css('padding-top', '12px');
                                                                         */


                                                                    })
        </script>

        <script>
            var modal = document.getElementById("myModal");
            var modalCaraPembayaran = document.getElementById("myModal-carapembayaran");

            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
                if (event.target == modalCaraPembayaran) {
                    modalCaraPembayaran.style.display = "none";
                }
            }


            function ShowModal(id)
            {
                var modal = document.getElementById(id);
                modal.style.display = "block";
            }
            
            function ShowModalCaraPembayaran()
            {
                var modalss = document.getElementById('myModal-carapembayaran');
                modalss.style.display = "block";
            }

            function CloseModal(id)
            {
                var modal = document.getElementById(id);
                modal.style.display = "none";
            }
            
            function CloseModalcarapembayaran()
            {
//                var modals = document.getElementById('modalCaraPembayaran');
                modalCaraPembayaran.style.display = "none";
            }
        </script>

        <script>
            var modalDp = document.getElementById("myModalDp");

            var img = document.getElementById("imageTampak1");
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function () {
                modalDp.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            var span = document.getElementsByClassName("closeDp")[0];

            span.onclick = function () {
                modalDp.style.display = "none";
            }
        </script>
    </body>

</html>
