<?php

class M_login extends CI_Model {

    function check_login($where) {
        return $this->db->get_where('mst_users',$where);
    }
    
    function update_forgot_password($data, $email){
        $this->db->where('email', $email);
        $query = $this->db->update('mst_users', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    function update($data, $id){
        $this->db->where('id_user', $id);
        $query = $this->db->update('mst_users', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

}