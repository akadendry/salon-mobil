<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_login');
    }

    public function index() {
        $this->load->view('login');
    }

    public function process_login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $array_data = array(
            'email' => $email,
            'password' => sha1(md5($password))
        );
        $check = $this->M_login->check_login($array_data)->result();
        if (count($check) > 0) {
            foreach ($check as $row):
                if ($row->verified == 0) {
                    $this->session->set_flashdata('status', 'danger');
                    $this->session->set_flashdata('message', 'User belum verifikasi, mohon lakukan verifikasi untuk masuk');
                    return redirect('login');
                }
                $session_data = array(
                    'id' => $row->id_user,
                    'name' => $row->name,
                    'email' => $row->email,
                );
                $this->session->set_userdata($session_data);
            endforeach;
            return redirect('home');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Username dan Password Salah');
            return redirect('login');
        }
    }

    public function process_logout() {
        session_destroy();
        unset($_SESSION);
        return redirect('home');
    }

    public function forgot_password() {
        $this->data['email'] = '';
        $this->load->view('forgot_password', $this->data);
    }

    public function process_forgot() {
        $email = $this->input->post('email');
        $this->data['email'] = $email;
        $array_data = array(
            'email' => $email
        );
        $check = $this->M_login->check_login($array_data)->result();
        if (count($check) > 0) {
            foreach ($check as $row):
                if ($row->verified == 0) {
                    $this->session->set_flashdata('status', 'danger');
                    $this->session->set_flashdata('message', 'User belum verifikasi, mohon lakukan verifikasi untuk masuk');
                    return redirect('login/forgot_password');
                }
            endforeach;

            $forgot_token = $this->generateRandomString(255);
            $data_update = array(
                'token_forgot' => $forgot_token
            );
            $update_tokenForgot = $this->M_login->update_forgot_password($data_update, $email);
            if ($update_tokenForgot == 1) {
                //kirim email
                $config = Array(
                    'smtp_host' => 'mail.detailingshopgarage.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'no-reply@detailingshopgarage.com', // change it to yours
                    'smtp_auth' => TRUE,
                    'smtp_pass' => 'Password1', // change it to yours
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'newline' => "\r\n",
                    'wordwrap' => TRUE
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from('no-reply@detailingshopgarage.com', 'DSG Indonesia');
                $this->email->to($email); // change it to yours
                $this->email->subject('Ubah Password Detailing Shop Garage');
                $data['recipient_email'] = $email;
                $data['url'] = base_url() . 'login/forgot_password_form/' . sha1(md5($email));
                $data['subjek_email'] = "Ubah Password";
                $email = $this->load->view('v_email_template_forgot_password', $data, TRUE);
                $this->email->message($email);
                if ($this->email->send()) {
                    $this->session->set_flashdata('status', 'success');
                    $this->session->set_flashdata('message', 'Link ubah password sudah dikirim ke email, silahkan dicek. . .');
                    return redirect('login/forgot_password', $this->data);
                } else {
                    $this->session->set_flashdata('status', 'danger');
                    $this->session->set_flashdata('message', 'Terjadi kesalahan sistem, mohon dicoba kembali!');
                    return redirect('login/forgot_password', $this->data);
                }
            } else {
                $this->session->set_flashdata('status', 'danger');
                $this->session->set_flashdata('message', 'Terjadi kesalahan sistem, mohon dicoba kembali!');
                return redirect('login/forgot_password', $this->data);
            }
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Email belum terdaftar di sistem kami, mohon diperiksa kembali!');
            return redirect('login/forgot_password', $this->data);
        }
    }

    public function forgot_password_form() {
        $email_sha1 = $this->uri->segment(3);
        $this->data['email_sha1'] = $email_sha1;
        $array_data = array(
            'email_token' => $email_sha1
        );
        $check = $this->M_login->check_login($array_data)->result();
        if (count($check) > 0) {
            foreach ($check as $row):
                $this->data['id'] = $row->id_user;
            endforeach;
            $this->data['password'] = '';
            $this->data['re_password'] = '';
            $this->load->view('forgot_password_form', $this->data);
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('login');
        }
    }

    public function process_forgot_submit() {
        $user_id = $this->input->post('user_id');
        $email_sha1 = $this->input->post('email_sha1');
        $password = $this->input->post('password');
        $re_password = $this->input->post('re_password');
        if ($password !== $re_password) {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Password dan ulangi password tidak sama, harap periksa kembali!');
            return redirect('login/forgot_password_form/' . $email_sha1);
        }
        $data_update = array(
            'password' => sha1(md5($password))
        );
        $update = $this->M_login->update($data_update, $user_id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Password berhasil diubah, silahkan masukan usernam dan password untuk masuk ke sistem kami. . .');
            return redirect('login');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('login/forgot_password_form/' . $email_sha1);
        }
    }

    function generateRandomString($length = 255) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}