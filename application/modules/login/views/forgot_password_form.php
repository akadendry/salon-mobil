

<!DOCTYPE html>
<html>
    <head>
        <title>Slide Login Form Flat Responsive Widget Template :: w3layouts</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Slide Login Form template Responsive, Login form web template, Flat Pricing tables, Flat Drop downs Sign up Web Templates, Flat Web Templates, Login sign up Responsive web template, SmartPhone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

        <script>
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
                window.scrollTo(0, 1);
            }
        </script>
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>

        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>assets/css/styles.css" rel='stylesheet' type='text/css' />	

        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">


        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
        <!-- //Custom Theme files -->

        <!-- web font -->

        <!-- //web font -->

    </head>
    <body>

        <!-- main -->
        <div class="w3layouts-main"> 
            <div class="bg-layer">
                <h1></h1>
                <div class="header-main">
                    <div class="main-icon" style="margin-top: -75px;">
                        <H1>Ubah Password</h1>
                    </div>
                    <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="header-left-bottom">
                        <form action="<?= base_url('login/process_forgot_submit') ?>" method="post">
                            <i style="color: white;font-family: teko;font-size: 23px; text-align: left;">Password Baru</i>

                            <div class="icon1">
                                <span class="fa fa-lock"></span>
                                <input type="hidden" name="user_id" value="<?= $id ?>"/>
                                <input type="hidden" name="email_sha1" value="<?= $email_sha1 ?>"/>
                                <input type="password" placeholder="Password" name="password" value="<?= $password; ?>" required/>
                            </div>
                            
                            <i style="color: white;font-family: teko;font-size: 23px; text-align: left;">Ulangi Password Baru</i>

                            <div class="icon1">
                                <span class="fa fa-lock"></span>
                                <input type="password" placeholder="Ulangi Password" name="re_password" value="<?= $re_password; ?>" required/>
                            </div>
                            <div class="bottom" style="padding-top: 20px;">
                                <button class="btn" style="height: 50px;">Submit</button>
                            </div>
                        </form>	
                    </div>

                </div>

                <!-- copyright -->
                <div class="copyright">
                    <p style="font-family: teko;">© 2019 DSG Indonesia . All rights reserved</p>
                </div>
                <!-- //copyright --> 
            </div>
        </div>	
        <!-- //main -->
        <script src="assets/js/jquery-2.1.4.min.js"></script>

    </body>
</html>