<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="http://app.malloci.com/eproc/acquis_gapura/assets/mailassets/font-awesome.min.css" />
        <link rel="stylesheet" href="http://app.malloci.com/eproc/acquis_gapura/assets/mailassets/emailtemplate.css" />
        <style>
                /* latin */
                @font-face {
                        font-family: 'Pacifico';
                        font-style: normal;
                        font-weight: 400;
                        src: local('Pacifico Regular'), local('Pacifico-Regular'), url(http://fonts.gstatic.com/s/pacifico/v7/Q_Z9mv4hySLTMoMjnk_rCRTbgVql8nDJpwnrE27mub0.woff2) format('woff2');
                        /*unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;*/
                }
        </style>
    </head>

    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Georgia, Times, serif">

        <!-- Wrapper -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="background-color: transparent;">

            <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="border-lr deviceWidth" background="http://app.malloci.com/eproc/acquis_gapura/assets/mailassets/bg_up_large.png" style="background-color: transparent;">
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                    <td align="right">
                        <h4 id="txt" style="text-align: right; color: #01839d; padding: 15px 32px 0px 32px; font-family: arial; text-align: center;"><?php echo $this->config->item('alias_sender'); ?></h4>
                    </td>
                </tr>   
            </table>

            <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="border-lr deviceWidth" bgcolor="#fff" style="background-color: transparent;" >

                <tr>
                    <td> 
                        <h4 id="judul" style="color: #01839d; padding-top: 32px; padding-bottom: 32px; text-align: center; font-family: arial;"><?php if (isset($subjek_email)): echo $subjek_email; else: echo "{Subjek}"; endif; ?></h4>
                        <p id="pembuka" style="padding: 0 32px;">
                            Kepada Yth. <br>
                            Pemilik <i>e-mail</i>&nbsp;<?php if (isset($recipient_email)): echo $recipient_email; else: echo "{alamat email}"; endif; ?><br>
                            di tempat.
                        </p>
                        <p id="penutup" style="padding: 0 32px;">
                            Untuk merubah password anda, silahkan klik tautan ini :  <a style="color: #01839d;" href="<?php echo $url; ?>" target="_blank">tautan</a>
                        </p>
                    </td>
                </tr>
            </table>
        </table> <!-- End Wrapper -->
    </body>
</html>
