<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {
            }
        </script>
        <ul class="nav nav-list">
            <?php if ($subMenu == 'notYet' || $subMenu == 'waitingApproval' || $subMenu == 'approval') : ?>
                <li class="active open">
            <?php else : ?>
                <li>
            <?php endif; ?>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Pembayaran DP </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="<?php if($subMenu == 'notYet'){echo 'active';}?>">
                        <a href="<?= base_url('dashboard') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Belum Bayar
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="<?php if($subMenu == 'waitingApproval'){echo 'active';}?>">
                        <a href="<?= base_url('dashboard/waitApproval') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Wait For Approval
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="<?php if($subMenu == 'approval'){echo 'active';}?>">
                        <a href="<?= base_url('dashboard/approval') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Approval
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            
            <?php if ($subMenu == 'precheck' || $subMenu == 'proses' || $subMenu == 'selesai') : ?>
                <li class="active open">
            <?php else : ?>
                <li>
            <?php endif; ?>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Status Mobil </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="<?php if($subMenu == 'precheck'){echo 'active';}?>">
                        <a href="<?= base_url('dashboard/precheck') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Precheck
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="<?php if($subMenu == 'proses'){echo 'active';}?>">
                        <a href="<?= base_url('dashboard/proses') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Proses
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="<?php if($subMenu == 'selesai'){echo 'active';}?>">
                        <a href="<?= base_url('dashboard/selesai') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Selesai
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            <?php if ($subMenu == 'history') : ?>
                <li class="active">
                <?php else : ?>
                <li>
                <?php endif; ?>
                <a href="<?= base_url('dashboard/history') ?>">
                    <i class="menu-icon fa fa-database"></i>
                    <span class="menu-text"> Riwayat Service </span>
                </a>
                <b class="arrow"></b>
            </li>
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
